package com.anikiki.tdf.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Ana on 7/8/2014.
 */
public class TdfDbOpenHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "tdf.db";
    private static final int DATABASE_VERSION = 1;

    // Table, columns
    public static final String TABLE_ATTRACTIONS = "attractions";
    public static final String COLUMN_UU_ID = "uuid";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_SELECTED_MARKER_ID = "sel_marker_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_LAT = "lat";
    public static final String COLUMN_LON = "lon";

    // Creation statement
    private static final String CREATE_TABLE_ATTRACTIONS = "CREATE TABLE "
            + TABLE_ATTRACTIONS + "("
            + COLUMN_UU_ID + " TEXT UNIQUE ON CONFLICT REPLACE, "
            + COLUMN_ID + " TEXT, "
            + COLUMN_SELECTED_MARKER_ID + " TEXT, "
            + COLUMN_NAME + " TEXT, "
            + COLUMN_LAT + " REAL, "
            + COLUMN_LON + " REAL);";

    private static final String ATTRACTIONS_SEL_MARKER_ID_INDEX_NAME = "attractions_sel_marker_id_idx";
    private static final String CREATE_ATTRACTIONS_SELECTED_MARKER_ID_INDEX = "CREATE INDEX " + ATTRACTIONS_SEL_MARKER_ID_INDEX_NAME + " ON " + TABLE_ATTRACTIONS + " (" + COLUMN_SELECTED_MARKER_ID + ");";

    private static final String ATTRACTIONS_ID_INDEX_NAME = "attractions_id_idx";
    private static final String CREATE_ATTRACTIONS_ID_INDEX = "CREATE INDEX " + ATTRACTIONS_ID_INDEX_NAME + " ON " + TABLE_ATTRACTIONS + " (" + COLUMN_ID + ");";

    public TdfDbOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_ATTRACTIONS);
        db.execSQL(CREATE_ATTRACTIONS_SELECTED_MARKER_ID_INDEX);
        db.execSQL(CREATE_ATTRACTIONS_ID_INDEX);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
