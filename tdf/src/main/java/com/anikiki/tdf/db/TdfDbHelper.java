package com.anikiki.tdf.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.anikiki.tdf.models.Attraction;
import com.anikiki.tdf.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by Ana on 7/8/2014.
 */
public class TdfDbHelper {
    private final static String TAG = TdfDbHelper.class.getSimpleName();

    private ExecutorService executor = Executors.newFixedThreadPool(1);
    private final SQLiteDatabase sqlite;

    private static final String[] ATTRACTIONS_COLUMN_NAMES = new String[]{
            TdfDbOpenHelper.COLUMN_UU_ID,
            TdfDbOpenHelper.COLUMN_NAME,
            TdfDbOpenHelper.COLUMN_ID,
            TdfDbOpenHelper.COLUMN_SELECTED_MARKER_ID,
            TdfDbOpenHelper.COLUMN_LAT,
            TdfDbOpenHelper.COLUMN_LON};

    public TdfDbHelper(TdfDbOpenHelper tdfDbOpenHelper) {
        sqlite = tdfDbOpenHelper.getWritableDatabase();
    }

    /**
     * Inserts attraction in the db.
     * <p/>
     * A 'Future<Long>' is used in the return instead of 'Long' in order to allow the current thread to wait
     * until there is a result.
     *
     * @param attractions      List of attractions around selected marker.
     * @param selectedMarkerId Selected marker id.
     * @return Number of rows inserted.
     */
    public Future<Long> insertOrReplace(final List<Attraction> attractions, final String selectedMarkerId) {
        return executor.submit(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                long insertedRows = 0;
                try {
                    sqlite.beginTransaction();
                    for (Attraction attraction : attractions) {
                        if (sqlite.insertWithOnConflict(TdfDbOpenHelper.TABLE_ATTRACTIONS, null, attraction.toContentValues(selectedMarkerId),
                                SQLiteDatabase.CONFLICT_REPLACE) >= 0) {
                            insertedRows++;
                        }
                    }
                    Log.d(TAG, "Insert succeeded, inserted rows:" + insertedRows);
                    sqlite.setTransactionSuccessful();
                    return insertedRows;
                } finally {
                    sqlite.endTransaction();
                }
            }
        });
    }

    /**
     * Gets from db a list containing all attractions around selected marker.
     * <p/>
     * This method uses internally {@link com.anikiki.tdf.db.TdfDbHelper#readAllAttractionsForSelectedMarkerFuture} which
     * returns a 'Future<Cursor>' instead of 'Cursor' in order to allow the current thread to wait
     * until there is a result.
     *
     * @param markerId Selected marker id.
     * @return List of attractions.
     */
    public List<Attraction> readAllAttractionsForSelectedMarker(final String markerId) {
        List<Attraction> attractions = new ArrayList<Attraction>();
        Cursor allAttractions = null;
        try {
            allAttractions = readAllAttractionsForSelectedMarkerFuture(markerId).get();
            if (allAttractions != null) {
                if (allAttractions.moveToFirst()) {
                    while (!allAttractions.isAfterLast()) {
                        Attraction attraction = new Attraction();
                        attraction.setName(allAttractions.getString(allAttractions.getColumnIndex(TdfDbOpenHelper.COLUMN_NAME)));
                        String lat = allAttractions.getString(allAttractions.getColumnIndex(TdfDbOpenHelper.COLUMN_LAT));
                        String lon = allAttractions.getString(allAttractions.getColumnIndex(TdfDbOpenHelper.COLUMN_LON));
                        attraction.setPosition(Utils.getLatLngFromStrings(lat, lon));
                        attractions.add(attraction);
                        allAttractions.moveToNext();
                    }
                }
            }
        } catch (InterruptedException e) {
            Log.e(TAG, e.getMessage(), e);
        } catch (ExecutionException e) {
            Log.e(TAG, e.getMessage(), e);
        } finally {
            if (allAttractions != null) {
                allAttractions.close();
            }
        }
        return attractions;
    }

    /**
     * Reads from db all attractions around selected marker.
     * <p/>
     * A 'Future<Cursor>' is used in the return instead of 'Cursor' in order to allow the current thread to wait
     * until there is a result.
     *
     * @param markerId Selected marker id.
     * @return Cursor containing all attractions aroud selected marker.
     */
    private Future<Cursor> readAllAttractionsForSelectedMarkerFuture(final String markerId) {
        return executor.submit(new Callable<Cursor>() {
            @Override
            public Cursor call() throws Exception {
                return sqlite.query(TdfDbOpenHelper.TABLE_ATTRACTIONS, ATTRACTIONS_COLUMN_NAMES,
                        TdfDbOpenHelper.COLUMN_SELECTED_MARKER_ID + " =  ?",
                        new String[]{markerId},
                        null,
                        null,
                        TdfDbOpenHelper.COLUMN_NAME + " ASC"
                );
            }
        });
    }
}
