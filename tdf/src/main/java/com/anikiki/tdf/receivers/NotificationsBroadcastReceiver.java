package com.anikiki.tdf.receivers;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.anikiki.tdf.R;
import com.anikiki.tdf.activities.MainActivity;
import com.anikiki.tdf.application.TdfApplication;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Ana on 7/26/2014.
 */
public class NotificationsBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = NotificationsBroadcastReceiver.class.getSimpleName();
    private static final String SIMPLE_DATE_PATTERN = "dd MMM 'at' HH:mm";

    public static final String STAGE_NAME = "stage_name";
    public static final String STAGE_START = "stage_start";


    @Override
    public void onReceive(Context context, Intent intent) {
        int notificationId = 001;
        Intent viewIntent = new Intent(context, MainActivity.class);
        PendingIntent viewPendingIntent = PendingIntent.getActivity(context, 0, viewIntent, 0);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(intent.getStringExtra(STAGE_NAME))
                .setContentText(context.getString(R.string.msg_about_to_start))
                .setContentIntent(viewPendingIntent);
        long currentStartDate = intent.getLongExtra(STAGE_START, 0);
        SimpleDateFormat simpleSdf = new SimpleDateFormat(SIMPLE_DATE_PATTERN);
        Date startDate = new Date();
        startDate.setTime(currentStartDate);
        Notification secondPageNotification = new NotificationCompat.Builder(context)
                .setContentTitle(intent.getStringExtra(STAGE_NAME))
                .setContentText(context.getString(R.string.msg_will_start) + " " + simpleSdf.format(startDate))
                .build();
        new NotificationCompat.WearableExtender()
                .addPage(secondPageNotification)
                .extend(notificationBuilder)
                .build();
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(notificationId, notificationBuilder.build());
    }
}
