package com.anikiki.tdf.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.anikiki.tdf.models.Attraction;
import com.anikiki.tdf.utils.MapUtils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

/**
 * Created by Ana on 7/17/2014.
 */
public class AttractionsBroadcastReceiver extends BroadcastReceiver {
    public static final String ATTRACTIONS_LIST = "selected_marker";
    private GoogleMap map;

    @Override
    public void onReceive(Context context, Intent intent) {
        ArrayList<Attraction> attractions = intent.getParcelableArrayListExtra(ATTRACTIONS_LIST);
        if (map != null) {
            LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
            for (Attraction attraction : attractions) {
                MarkerOptions marker = new MarkerOptions().position(attraction.getPosition()).title(attraction.getName()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                map.addMarker(marker);
                boundsBuilder.include(attraction.getPosition());
            }
            MapUtils.updateCamera(map, boundsBuilder);
        }
    }

    public void setMap(GoogleMap map) {
        this.map = map;
    }
}
