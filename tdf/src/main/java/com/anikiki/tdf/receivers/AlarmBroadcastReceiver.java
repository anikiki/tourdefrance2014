package com.anikiki.tdf.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.anikiki.tdf.application.TdfApplication;
import com.anikiki.tdf.application.TdfPreferences;

/**
 * Created by Ana on 7/26/2014.
 */
public class AlarmBroadcastReceiver extends BroadcastReceiver {
    private static final int TOTAL_STAGES = 3;
    public static final String STAGE_NAME = "stage_name";
    public static final String STAGE_START = "stage_start";

    @Override
    public void onReceive(Context context, Intent intent) {
        TdfPreferences prefs = ((TdfApplication) context.getApplicationContext()).getTdfPreferences();
        boolean checkNextReminder = true;
        for (int i = 0; i < TOTAL_STAGES; i++) {
            if (checkNextReminder) {
                long time = getTimeFromPrefs(i, prefs);
                if (time > 0) {
                    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                    Intent alarmIntent = new Intent(context, NotificationsBroadcastReceiver.class);
                    alarmIntent.putExtra(NotificationsBroadcastReceiver.STAGE_NAME, intent.getStringExtra(STAGE_NAME));
                    alarmIntent.putExtra(NotificationsBroadcastReceiver.STAGE_START, intent.getStringExtra(STAGE_START));
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    if (time > System.currentTimeMillis()) {
                        alarmManager.set(AlarmManager.RTC_WAKEUP, time, pendingIntent);
                        checkNextReminder = false;
                    }
                }
            }
        }
    }

    private long getTimeFromPrefs(int stageNumber, TdfPreferences prefs) {
        long time = prefs.getStartStageReminderTime(stageNumber);
        if (System.currentTimeMillis() > time) {
            time = 0;
        }
        return time;
    }
}