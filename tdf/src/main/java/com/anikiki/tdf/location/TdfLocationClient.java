package com.anikiki.tdf.location;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

/**
 * Created by Ana on 7/23/2014.
 */
public class TdfLocationClient {
    private static final String TAG = TdfLocationClient.class.getSimpleName();
    private static final int LOCATION_FASTEST_UPDATE_INTERVAL = 1000;
    private static final long LOCATION_UPDATE_INTERVAL = 5000;

    private LocationRequest locationRequest;
    private LocationClient locationClient;
    private Location lastReadLocation;

    private GooglePlayServicesClient.ConnectionCallbacks connectionCallbacks = new GooglePlayServicesClient.ConnectionCallbacks() {
        @Override
        public void onConnected(Bundle bundle) {
            Log.i(TAG, "Connected to location services.");
            locationClient.requestLocationUpdates(locationRequest, locationListener);
        }

        @Override
        public void onDisconnected() {
            Log.i(TAG, "Disconnected from location services.");
        }
    };

    private GooglePlayServicesClient.OnConnectionFailedListener connectionFailedListener = new GooglePlayServicesClient.OnConnectionFailedListener() {
        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            Log.i(TAG, "Connection to location services failed.");
        }
    };

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            lastReadLocation = location;
            Log.i(TAG, "New location retrieved from location services.");
        }
    };

    public Location getLastReadLocation() {
        return lastReadLocation;
    }

    public void connect() {
        locationClient.connect();
    }

    public TdfLocationClient(Context context) {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(LOCATION_UPDATE_INTERVAL);
        locationRequest.setFastestInterval(LOCATION_FASTEST_UPDATE_INTERVAL);
        locationClient = new LocationClient(context, connectionCallbacks, connectionFailedListener);
    }

    public void disconnect() {
        if (locationClient.isConnected()) {
            locationClient.removeLocationUpdates(locationListener);
            locationClient.disconnect();
        }
    }
}
