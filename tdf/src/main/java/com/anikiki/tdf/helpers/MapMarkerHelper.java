package com.anikiki.tdf.helpers;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by Ana on 7/10/2014.
 */
public class MapMarkerHelper {
    private static final String URI_NO_CURRENT_LOCATION = "geo:0,0?q=%f,%f(%s)";
    private static final String URI_EXISTING_CURRENT_LOCATION = "http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f(%s)";
    private boolean markerClicked = false;
    private Marker selectedMarker;

    public void setMapMarkerListeners(GoogleMap map, final Activity activity) {
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                markerClicked = true;
                selectedMarker = marker;
                activity.invalidateOptionsMenu();
                return false;
            }
        });
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                markerClicked = false;
                selectedMarker = null;
                activity.invalidateOptionsMenu();
            }
        });
    }

    public void startNavigation(Activity activity, Location location) {
        if (getSelectedMarker() != null) {
            Uri uri = null;
            if (location == null) {
                uri = Uri.parse(String.format(URI_NO_CURRENT_LOCATION,
                        getSelectedMarker().getPosition().latitude,
                        getSelectedMarker().getPosition().longitude,
                        getSelectedMarker().getTitle()));
            } else {
                uri = Uri.parse(String.format(URI_EXISTING_CURRENT_LOCATION,
                        location.getLatitude(),
                        location.getLongitude(),
                        getSelectedMarker().getPosition().latitude,
                        getSelectedMarker().getPosition().longitude,
                        getSelectedMarker().getTitle()));
            }
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
            activity.startActivity(intent);
        }
    }

    public boolean isMarkerClicked() {
        return markerClicked;
    }

    public Marker getSelectedMarker() {
        return selectedMarker;
    }
}
