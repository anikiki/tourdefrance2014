package com.anikiki.tdf.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ana on 7/4/2014.
 */
public class Rider implements Parcelable {
    private String name;
    private String country;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(country);
    }

    public static final Creator<Rider> CREATOR = new Creator<Rider>() {
        @Override
        public Rider createFromParcel(Parcel source) {
            return new Rider(source);
        }

        @Override
        public Rider[] newArray(int size) {
            return new Rider[size];
        }
    };

    private Rider(Parcel source) {
        name = source.readString();
        country = source.readString();
    }

    public Rider() {}
}
