package com.anikiki.tdf.models;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;

import com.anikiki.tdf.db.TdfDbOpenHelper;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Ana on 7/7/2014.
 */
public class Attraction implements Parcelable {
    private String uuid;
    private String id;
    private String name;
    private LatLng position;

    public ContentValues toContentValues(String selectedPosition) {
        ContentValues cv = new ContentValues();
        cv.put(TdfDbOpenHelper.COLUMN_UU_ID, getUuid());
        cv.put(TdfDbOpenHelper.COLUMN_NAME, getName());
        cv.put(TdfDbOpenHelper.COLUMN_ID, getId());
        cv.put(TdfDbOpenHelper.COLUMN_SELECTED_MARKER_ID, selectedPosition);
        cv.put(TdfDbOpenHelper.COLUMN_LAT, getPosition().latitude);
        cv.put(TdfDbOpenHelper.COLUMN_LON, getPosition().longitude);
        return cv;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LatLng getPosition() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uuid);
        dest.writeString(id);
        dest.writeString(name);
        dest.writeParcelable(position, flags);
    }

    private static final Creator<Attraction> CREATOR = new Creator<Attraction>() {
        @Override
        public Attraction createFromParcel(Parcel parcel) {
            return new Attraction(parcel);
        }

        @Override
        public Attraction[] newArray(int i) {
            return new Attraction[i];
        }
    };

    private Attraction(Parcel source) {
        uuid = source.readString();
        id = source.readString();
        name = source.readString();
        position = source.readParcelable(LatLng.class.getClassLoader());
    }

    public Attraction() {
    }
}
