package com.anikiki.tdf.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Ana on 7/4/2014.
 */
public class SpectatorHub implements Parcelable {
    private String name;
    private String capacity;
    private String facilities;
    private LatLng latLong;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getFacilities() {
        return facilities;
    }

    public void setFacilities(String facilities) {
        this.facilities = facilities;
    }

    public LatLng getLatLong() {
        return latLong;
    }

    public void setLatLong(LatLng latLong) {
        this.latLong = latLong;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(capacity);
        dest.writeString(facilities);
        dest.writeParcelable(latLong, flags);
    }

    public static final Creator<SpectatorHub> CREATOR = new Creator<SpectatorHub>() {
        @Override
        public SpectatorHub createFromParcel(Parcel source) {
            return new SpectatorHub(source);
        }

        @Override
        public SpectatorHub[] newArray(int size) {
            return new SpectatorHub[size];
        }
    };

    private SpectatorHub(Parcel source) {
        name = source.readString();
        capacity = source.readString();
        facilities = source.readString();
        latLong = source.readParcelable(LatLng.class.getClassLoader());
    }

    public SpectatorHub() {
    }
}
