package com.anikiki.tdf.models;

import java.util.List;

/**
 * Created by Ana on 7/4/2014.
 */
public class Stage {
    private int number;
    private String name;
    private String length;
    private String startDate;
    private String startTime;
    private long timestamp;
    private List<Hill> hills;
    private List<Sprint> sprints;
    private List<SpectatorHub> spectatorHubs;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public List<Hill> getHills() {
        return hills;
    }

    public void setHills(List<Hill> hills) {
        this.hills = hills;
    }

    public List<Sprint> getSprints() {
        return sprints;
    }

    public void setSprints(List<Sprint> sprints) {
        this.sprints = sprints;
    }

    public List<SpectatorHub> getSpectatorHubs() {
        return spectatorHubs;
    }

    public void setSpectatorHubs(List<SpectatorHub> spectatorHubs) {
        this.spectatorHubs = spectatorHubs;
    }
}
