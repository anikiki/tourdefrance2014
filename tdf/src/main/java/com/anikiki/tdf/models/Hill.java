package com.anikiki.tdf.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Ana on 7/4/2014.
 */
public class Hill implements Parcelable {
    private String name;
    private LatLng latLong;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LatLng getLatLong() {
        return latLong;
    }

    public void setLatLong(LatLng latLong) {
        this.latLong = latLong;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeParcelable(latLong, flags);
    }

    public static final Creator<Hill> CREATOR = new Creator<Hill>() {
        @Override
        public Hill createFromParcel(Parcel source) {
            return new Hill(source);
        }

        @Override
        public Hill[] newArray(int size) {
            return new Hill[size];
        }
    };

    private Hill(Parcel source) {
        name = source.readString();
        latLong = source.readParcelable(LatLng.class.getClassLoader());
    }

    public Hill() {
    }
}
