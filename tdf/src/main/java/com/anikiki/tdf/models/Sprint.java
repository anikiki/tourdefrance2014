package com.anikiki.tdf.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Ana on 7/4/2014.
 */
public class Sprint implements Parcelable {
    private String name;
    private LatLng latLong;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LatLng getLatLong() {
        return latLong;
    }

    public void setLatLong(LatLng latLong) {
        this.latLong = latLong;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeParcelable(latLong, flags);
    }

    public static final Creator<Sprint> CREATOR = new Creator<Sprint>() {
        @Override
        public Sprint createFromParcel(Parcel source) {
            return new Sprint(source);
        }

        @Override
        public Sprint[] newArray(int size) {
            return new Sprint[size];
        }
    };

    private Sprint(Parcel source) {
        name = source.readString();
        latLong = source.readParcelable(LatLng.class.getClassLoader());
    }

    public Sprint() {
    }
}
