package com.anikiki.tdf.activities;

import android.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.anikiki.tdf.R;
import com.anikiki.tdf.fragments.FragmentNavigationListener;
import com.anikiki.tdf.fragments.HandleBackKey;
import com.anikiki.tdf.fragments.StageFragment;
import com.anikiki.tdf.fragments.StageOptionsMapFragment;
import com.anikiki.tdf.fragments.StagePassesAndSprintsFragment;
import com.anikiki.tdf.fragments.StageProfileFragment;
import com.anikiki.tdf.fragments.StageSpectatorHubsFragment;
import com.anikiki.tdf.fragments.StageVideoFragment;
import com.anikiki.tdf.models.SpectatorHub;

/**
 * Created by Ana on 7/4/2014.
 */
public class StageActivity extends BaseActivity {
    private static final String TAG = StageActivity.class.getSimpleName();
    private static final int HOME_ITEM_POSITION = 0;
    private static final int MAP_ITEM_POSITION = 1;
    private static final int INFO_ITEM_POSITION = 2;
    private static final int PROFILE_ITEM_POSITION = 3;
    private static final int PASSES_AND_SPRINTS_ITEM_POSITION = 4;
    private static final int SPECTATOR_HUBS_ITEM_POSITION = 5;
    private static final int VIDEO_ITEM_POSITION = 6;
    private static final String DRAWER_SELECTED_ITEM_POSITION = "drawer_selected_item_position";
    private static final String OPTIONS_MAP_FRAGMENT_TAG = "options_map_fragment_tag";
    private static final String FRAGMENT_CLASS = "fragment_class_name";

    public static final String ROUTE_FILE_ID = "route_file_id";
    public static final String VIDEO_ID = "video_id";
    public static final String STAGE_NUMBER = "stage_number";
    public static final String STAGE_NAME = "stage_name";

    private int drawerLastSelectedItemPosition = MAP_ITEM_POSITION;
    private int stageNumber;
    private String stageName;
    private DrawerLayout stageDrawerLayout;
    private ListView drawerList;
    private ActionBarDrawerToggle drawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // called before super.onCreate() as requestFeature() must be called before adding content
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stage_activity);
        String[] menu = new String[]{
                getString(R.string.menu_item_home),
                getString(R.string.menu_item_map),
                getIntent().getStringExtra(STAGE_NAME),
                getString(R.string.menu_item_profile),
                getString(R.string.menu_item_passes_sprints),
                getString(R.string.menu_item_spectator_hubs),
                getString(R.string.menu_item_video)};
        stageDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerList = (ListView) findViewById(R.id.left_drawer);
        stageDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        drawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, menu));
        drawerList.setOnItemClickListener(new DrawerItemClickListener());
        drawerToggle = createActionBarDrawerToggle();
        stageNumber = getIntent().getIntExtra(STAGE_NUMBER, 0);
        stageName = getIntent().getStringExtra(STAGE_NAME);

        if (savedInstanceState != null) {
            drawerLastSelectedItemPosition = savedInstanceState.getInt(DRAWER_SELECTED_ITEM_POSITION);
        }
        selectItem(drawerLastSelectedItemPosition);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return drawerToggle.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Fragment currentFragment = getCurrentFragment();
            if (currentFragment instanceof HandleBackKey) {
                boolean result = ((HandleBackKey) currentFragment).handleBackKey();
                return result ? result : super.onKeyDown(keyCode, event);
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(DRAWER_SELECTED_ITEM_POSITION, drawerLastSelectedItemPosition);
        super.onSaveInstanceState(outState);
    }

    private Bundle createStageInfoBundle(String fragmentClassName, String stageNumberKey) {
        Bundle stageInfoBundle = new Bundle();
        stageInfoBundle.putInt(stageNumberKey, stageNumber);
        stageInfoBundle.putString(STAGE_NAME, stageName);
        stageInfoBundle.putString(FRAGMENT_CLASS, fragmentClassName);
        return stageInfoBundle;
    }

    private void startHome() {
        finish();
    }

    private void startInfo() {
        StageFragment stageInfoFragment = new StageFragment();
        stageInfoFragment.setListener(fragmentNavigationListener);
        stageInfoFragment.setArguments(createStageInfoBundle(StageFragment.class.getCanonicalName(), StageFragment.STAGE_NUMBER));
        getFragmentManager().beginTransaction().replace(R.id.stage_container, stageInfoFragment).commit();
        getActionBar().setTitle(getIntent().getStringExtra(STAGE_NAME));
    }

    private void startMap(SpectatorHub spectatorHub, int position, boolean animateCamera) {
        Bundle bundle = createStageInfoBundle(StageOptionsMapFragment.class.getCanonicalName(), StageOptionsMapFragment.STAGE_NUMBER);
        bundle.putInt(StageOptionsMapFragment.ROUTE_FILE_ID, getIntent().getIntExtra(ROUTE_FILE_ID, R.raw.routeone));
        bundle.putBoolean(StageOptionsMapFragment.ANIMATE_CAMERA, animateCamera);
        if (spectatorHub != null) {
            bundle.putParcelable(StageOptionsMapFragment.HUB_MARKER, spectatorHub);
            bundle.putInt(StageOptionsMapFragment.HUB_MARKER_POSITION, position);
        }
        StageOptionsMapFragment optionsMapFragment = (StageOptionsMapFragment) getFragmentManager().findFragmentByTag(OPTIONS_MAP_FRAGMENT_TAG);
        if (optionsMapFragment == null) {
            optionsMapFragment = new StageOptionsMapFragment();
            optionsMapFragment.setListener(stageMapListener).setListener(fragmentNavigationListener);
            optionsMapFragment.setArguments(bundle);
            getFragmentManager().beginTransaction().replace(R.id.stage_container, optionsMapFragment, OPTIONS_MAP_FRAGMENT_TAG).commit();
        }
        getActionBar().setTitle(getString(R.string.menu_item_map));
    }

    private void startProfile() {
        Bundle bundle = createStageInfoBundle(StageProfileFragment.class.getCanonicalName(), StageProfileFragment.STAGE_NUMBER);
        bundle.putInt(StageProfileFragment.ROUTE_FILE_ID, getIntent().getIntExtra(ROUTE_FILE_ID, R.raw.routeone));
        StageProfileFragment stageProfileFragment = new StageProfileFragment();
        stageProfileFragment.setListener(fragmentNavigationListener);
        stageProfileFragment.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.stage_container, stageProfileFragment).commit();
        getActionBar().setTitle(getString(R.string.menu_item_profile));
    }

    private void startPassesSprints() {
        StagePassesAndSprintsFragment stagePassesAndSprintsFragment = new StagePassesAndSprintsFragment();
        stagePassesAndSprintsFragment.setListener(fragmentNavigationListener);
        stagePassesAndSprintsFragment.setArguments(createStageInfoBundle(StagePassesAndSprintsFragment.class.getCanonicalName(), StagePassesAndSprintsFragment.STAGE_NUMBER));
        getFragmentManager().beginTransaction().replace(R.id.stage_container, stagePassesAndSprintsFragment).commit();
        getActionBar().setTitle(getString(R.string.menu_item_passes_sprints));
    }

    private void startSpectatorHubs(int preselectedHubIndex) {
        Bundle bundle = createStageInfoBundle(StageSpectatorHubsFragment.class.getCanonicalName(), StageSpectatorHubsFragment.STAGE_NUMBER);
        if (preselectedHubIndex != -1) {
            bundle.putInt(StageSpectatorHubsFragment.HUB_MARKER_POSITION, preselectedHubIndex);
        }
        bundle.putInt(StageSpectatorHubsFragment.ROUTE_FILE_ID, getIntent().getIntExtra(ROUTE_FILE_ID, R.raw.routeone));
        StageSpectatorHubsFragment stageInfoFragment = new StageSpectatorHubsFragment();
        stageInfoFragment.setListener(stageSpectatorHubsListener);
        stageInfoFragment.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.stage_container, stageInfoFragment).commit();
        getActionBar().setTitle(getString(R.string.menu_item_spectator_hubs));
    }

    private void startVideo() {
        Bundle bundle = createStageInfoBundle(StageVideoFragment.class.getCanonicalName(), StageVideoFragment.STAGE_NUMBER);
        bundle.putString(VIDEO_ID, getIntent().getStringExtra(StageVideoFragment.VIDEO_ID));
        StageVideoFragment youTubePlayerFragment = new StageVideoFragment();
        youTubePlayerFragment.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.stage_container, youTubePlayerFragment).commit();
        getActionBar().setTitle(getString(R.string.menu_item_video));
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            drawerLastSelectedItemPosition = position;
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        switch (position) {
            case HOME_ITEM_POSITION:
                startHome();
                break;
            case MAP_ITEM_POSITION:
                startMap(null, -1, true);
                break;
            case INFO_ITEM_POSITION:
                startInfo();
                break;
            case PROFILE_ITEM_POSITION:
                startProfile();
                break;
            case PASSES_AND_SPRINTS_ITEM_POSITION:
                startPassesSprints();
                break;
            case SPECTATOR_HUBS_ITEM_POSITION:
                startSpectatorHubs(0);
                break;
            case VIDEO_ITEM_POSITION:
                startVideo();
                break;
            default:
                startMap(null, -1, true);
        }
        drawerList.setItemChecked(position, true);
        stageDrawerLayout.closeDrawer(drawerList);
    }

    private ActionBarDrawerToggle createActionBarDrawerToggle() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, stageDrawerLayout, R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close);
        stageDrawerLayout.setDrawerListener(toggle);
        return toggle;
    }

    private Fragment getCurrentFragment() {
        return getFragmentManager().findFragmentById(R.id.stage_container);
    }

    StageSpectatorHubsFragment.StageSpectatorHubsListener stageSpectatorHubsListener = new StageSpectatorHubsFragment.StageSpectatorHubsListener() {
        @Override
        public void setSpectatorHubInfo(SpectatorHub spectatorHub, int position, boolean animateCamera) {
            startMap(spectatorHub, position, animateCamera);
        }
    };

    StageOptionsMapFragment.StageOptionsMapListener stageMapListener = new StageOptionsMapFragment.StageOptionsMapListener() {
        @Override
        public void onSelectStageSpectatorHub(int position) {
            startSpectatorHubs(position);
        }
    };

    FragmentNavigationListener fragmentNavigationListener = new FragmentNavigationListener() {
        @Override
        public void handleBackNavigationFromFragment() {
            stageDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
            stageDrawerLayout.openDrawer(Gravity.LEFT);
            stageDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
                @Override
                public void onDrawerSlide(View drawerView, float slideOffset) {
                }

                @Override
                public void onDrawerOpened(View drawerView) {
                    stageDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                    stageDrawerLayout.setDrawerListener(null);
                }

                @Override
                public void onDrawerClosed(View drawerView) {
                }

                @Override
                public void onDrawerStateChanged(int newState) {
                }
            });
        }
    };
}
