package com.anikiki.tdf.activities;

import android.os.Bundle;

import com.anikiki.tdf.R;
import com.anikiki.tdf.fragments.RidersFragment;

/**
 * Created by Ana on 7/5/2014.
 */
public class RidersActivity extends BaseActivity {
    public static final String RIDERS_INFO = "riders_info";
    public static final String RIDERS_TEAM_NAME = "riders_team_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.riders_activity);
        RidersFragment fragment = (RidersFragment) getFragmentManager().findFragmentById(R.id.list_riders_fragment);
        Bundle bundle = getIntent().getBundleExtra(RIDERS_INFO);
        fragment.updateRidersDisplay(bundle.getParcelableArrayList(RIDERS_INFO));
        getActionBar().setTitle(bundle.getString(RIDERS_TEAM_NAME));
    }
}
