package com.anikiki.tdf.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import com.anikiki.tdf.R;
import com.anikiki.tdf.adapters.items.ListItem;
import com.anikiki.tdf.fragments.RidersFragment;
import com.anikiki.tdf.fragments.TeamsFragment;
import com.anikiki.tdf.models.Rider;
import com.anikiki.tdf.models.Team;
import com.anikiki.tdf.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ana on 7/4/2014.
 */
public class TeamsAndRidersActivity extends BaseActivity implements TeamsFragment.TeamSelectedListener {
    private static final String TAG = TeamsAndRidersActivity.class.getSimpleName();
    private RidersFragment ridersFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teams_and_riders_activity);
        if (Utils.isLargeScreen(this)) {
            ridersFragment = (RidersFragment) getFragmentManager().findFragmentById(R.id.list_riders_fragment);
        }
    }

    @Override
    public void onTeamSelected(int position, List<ListItem> teams) {
        if (ridersFragment == null) {
            ridersFragment = new RidersFragment();
        }
        if (!Utils.isLargeScreen(this) || getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            Intent intent = new Intent(this, RidersActivity.class);
            Bundle ridersInfo = new Bundle();
            ridersInfo.putParcelableArrayList(RidersActivity.RIDERS_INFO, (ArrayList<Rider>) ((Team) (teams.get(position).getItem())).getRiders());
            ridersInfo.putString(RidersActivity.RIDERS_TEAM_NAME, ((Team) (teams.get(position).getItem())).getName());
            intent.putExtra(RidersActivity.RIDERS_INFO, ridersInfo);
            startActivity(intent);
        } else {
            ridersFragment.updateRidersDisplay(position, teams);
        }
    }
}
