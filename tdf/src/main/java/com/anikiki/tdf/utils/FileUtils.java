package com.anikiki.tdf.utils;

import android.content.Context;
import android.util.Log;

import com.anikiki.tdf.R;
import com.anikiki.tdf.adapters.items.ListItem;
import com.anikiki.tdf.adapters.items.teams.HeaderTeamItem;
import com.anikiki.tdf.adapters.items.teams.RowTeamItem;
import com.anikiki.tdf.models.Hill;
import com.anikiki.tdf.models.Rider;
import com.anikiki.tdf.models.SpectatorHub;
import com.anikiki.tdf.models.Sprint;
import com.anikiki.tdf.models.Stage;
import com.anikiki.tdf.models.Team;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Ana on 7/3/2014.
 */
public class FileUtils {
    private static final String TAG = FileUtils.class.getSimpleName();
    private static final String TEAMS = "teams";
    private static final String NAME = "name";
    private static final String COUNTRY = "country";
    private static final String STATUS = "status";
    private static final String SPONSOR = "sponsor";
    private static final String MANAGER = "manager";
    private static final String RIDERS = "riders";
    private static final String STAGES = "stages";
    private static final String LENGTH = "length";
    private static final String START_DATE = "start_date";
    private static final String START_TIME = "start_time";
    private static final String TIMESTAMP = "timestamp";
    private static final String HILLS = "hills";
    private static final String SPRINTS = "sprints";
    private static final String SPECTATOR_HUBS = "spectator_hubs";
    private static final String CAPACITY = "capacity";
    private static final String FACILITIES = "facilities";
    private static final String LAT = "lat";
    private static final String LON = "lon";
    private static final String ROOT_NODE = "trkseg";
    private static final String ELE_NODE = "ele";

    public static JSONObject convertFile(int id, Context context) {
        StringBuilder sb = new StringBuilder();
        InputStream inputStream = context.getResources().openRawResource(id);
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
        } finally {
            try {
                br.close();
                inputStream.close();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }
        JSONObject json = null;
        try {
            json = new JSONObject(sb.toString());
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return json;
    }

    public static List<ListItem> getTeams(JSONObject json, Context context) {
        List<ListItem> teams = new ArrayList<ListItem>();
        // create a team with all riders and add it on top of the list
        Team all = new Team();
        all.setName(context.getString(R.string.title_all_riders));
        List<Rider> allRiders = new ArrayList<Rider>();
        all.setRiders(allRiders);
        ListItem header = new HeaderTeamItem(context, all);
        teams.add(header);
        try {
            JSONArray array = json.getJSONArray(TEAMS);
            for (int i = 0; i < array.length(); i++) {
                JSONObject objTeam = array.getJSONObject(i);
                Team team = new Team();
                team.setName(objTeam.getString(NAME));
                team.setCountry(objTeam.getString(COUNTRY));
                team.setStatus(objTeam.getString(STATUS));
                team.setSponsor(objTeam.getString(SPONSOR));
                team.setManager(objTeam.getString(MANAGER));
                JSONArray arrayRiders = objTeam.getJSONArray(RIDERS);
                List<Rider> riders = new ArrayList<Rider>();
                for (int j = 0; j < arrayRiders.length(); j++) {
                    Rider rider = new Rider();
                    JSONObject objRider = arrayRiders.getJSONObject(j);
                    rider.setName(objRider.getString(NAME));
                    rider.setCountry(objRider.getString(COUNTRY));
                    riders.add(rider);
                }
                team.setRiders(riders);
                allRiders.addAll(riders);
                ListItem row = new RowTeamItem(context, team);
                teams.add(row);
            }
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return teams;
    }

    public static Stage getStageInfo(JSONObject json, int pos) {
        Stage stage = new Stage();
        try {
            JSONArray array = json.getJSONArray(STAGES);
            if (pos >= 0 && pos < array.length()) {
                JSONObject objStage = array.getJSONObject(pos);
                stage.setNumber(pos);
                stage.setName(objStage.getString(NAME));
                stage.setLength(objStage.getString(LENGTH));
                stage.setStartDate(objStage.getString(START_DATE));
                stage.setStartTime(objStage.getString(START_TIME));
                stage.setTimestamp(objStage.getLong(TIMESTAMP));
            }
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return stage;
    }

    public static Stage getPassesAndSprintsForStage(JSONObject json, int pos) {
        Stage stage = new Stage();
        try {
            JSONArray array = json.getJSONArray(STAGES);
            if (pos >= 0 && pos < array.length()) {
                JSONObject objStage = array.getJSONObject(pos);
                JSONArray arrayPasses = objStage.getJSONArray(HILLS);
                List<Hill> passes = new ArrayList<Hill>();
                for (int i = 0; i < arrayPasses.length(); i++) {
                    JSONObject objPass = arrayPasses.getJSONObject(i);
                    Hill hill = new Hill();
                    hill.setName(objPass.getString(NAME));
                    hill.setLatLong(Utils.getLatLngFromStrings(objPass.getString(LAT), objPass.getString(LON)));
                    passes.add(hill);
                }
                JSONArray arraySprints = objStage.getJSONArray(SPRINTS);
                List<Sprint> sprints = new ArrayList<Sprint>();
                for (int i = 0; i < arraySprints.length(); i++) {
                    JSONObject objSprint = arraySprints.getJSONObject(i);
                    Sprint sprint = new Sprint();
                    sprint.setName(objSprint.getString(NAME));
                    sprint.setLatLong(Utils.getLatLngFromStrings(objSprint.getString(LAT), objSprint.getString(LON)));
                    sprints.add(sprint);
                }
                stage.setHills(passes);
                stage.setSprints(sprints);
            }
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return stage;
    }

    public static Stage getSpectatorHubsForStage(JSONObject json, int pos) {
        Stage stage = new Stage();
        try {
            JSONArray array = json.getJSONArray(STAGES);
            if (pos >= 0 && pos < array.length()) {
                JSONObject objStage = array.getJSONObject(pos);
                JSONArray arrayHubs = objStage.getJSONArray(SPECTATOR_HUBS);
                List<SpectatorHub> hubs = new ArrayList<SpectatorHub>();
                for (int i = 0; i < arrayHubs.length(); i++) {
                    JSONObject objHub = arrayHubs.getJSONObject(i);
                    SpectatorHub hub = new SpectatorHub();
                    hub.setName(objHub.getString(NAME));
                    hub.setCapacity(objHub.getString(CAPACITY));
                    hub.setFacilities(objHub.getString(FACILITIES));
                    hub.setLatLong(Utils.getLatLngFromStrings(objHub.getString(LAT), objHub.getString(LON)));
                    hubs.add(hub);
                }
                stage.setSpectatorHubs(hubs);
            }
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return stage;
    }

    public static List<LatLng> getPointsFromFile(int id, Context context) {
        List<LatLng> list = new ArrayList<LatLng>();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        InputStream inputStream = null;
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            inputStream = context.getResources().openRawResource(id);
            Document doc = db.parse(inputStream);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName(ROOT_NODE).item(0).getChildNodes();
            if (nList != null) {
                for (int i = 0; i < nList.getLength(); i++) {
                    if (i % 7 == 0) {
                        Node node = nList.item(i);
                        if (node.getNodeType() == Node.ELEMENT_NODE) {
                            Element element = (Element) node;
                            LatLng ll = new LatLng(Double.parseDouble(element.getAttribute(LAT)), Double.parseDouble(element.getAttribute(LON)));
                            list.add(ll);
                        }
                    }
                }
            }
        } catch (ParserConfigurationException e) {
            Log.e(TAG, e.getMessage(), e);
        } catch (SAXException e) {
            Log.e(TAG, e.getMessage(), e);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    Log.e(TAG, e.getMessage(), e);
                }
            }
        }
        return list;
    }

    public static List<Number> getElevationPointsFromFile(int id, Context context) {
        List<Number> list = new ArrayList<Number>();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        InputStream inputStream = null;
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            inputStream = context.getResources().openRawResource(id);
            Document doc = db.parse(inputStream);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName(ROOT_NODE).item(0).getChildNodes();
            if (nList != null) {
                for (int i = 0; i < nList.getLength(); i++) {
                    if (i % 7 == 0) {
                        Node node = nList.item(i).getFirstChild();
                        if (node.getNodeType() == Node.ELEMENT_NODE && ELE_NODE.equals(node.getNodeName())) {
                            Element element = (Element) node;
                            Number number = Double.parseDouble(element.getFirstChild().getNodeValue());
                            list.add(number);
                        }
                    }
                }
            }
        } catch (ParserConfigurationException e) {
            Log.e(TAG, e.getMessage(), e);
        } catch (SAXException e) {
            Log.e(TAG, e.getMessage(), e);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    Log.e(TAG, e.getMessage(), e);
                }
            }
        }
        return list;
    }
}
