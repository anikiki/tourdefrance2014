package com.anikiki.tdf.utils;

import android.content.Context;
import android.util.Log;

import com.anikiki.tdf.R;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ana on 7/5/2014.
 */
public class Utils {
    private static final String TAG = Utils.class.getSimpleName();
    private static final String SCREEN_LARGE = "true";
    public static final int TIMEOUT_MILLISECONDS = 15000;
    public static final String API_KEY = "AIzaSyBKH9JJoKfcH-KbC0KFIySc_8FSbLqmzqc";

    public static boolean isLargeScreen(Context context) {
        return SCREEN_LARGE.equals(context.getString(R.string.large_screen_size));
    }

    public static String getStringFromJson(String name, JSONObject jObject) {
        String result = "";
        try {
            result = jObject.getString(name);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }
        return result;
    }

    public static LatLng getLatLngFromStrings(String lat, String lon) {
        double dLat = 0d;
        if (lat != null && !"".equals(lat)) {
            try {
                dLat = Double.parseDouble(lat);
            } catch (NumberFormatException e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }
        double dLon = 0d;
        if (lon != null && !"".equals(lon)) {
            try {
                dLon = Double.parseDouble(lon);
            } catch (NumberFormatException e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }
        return new LatLng(dLat, dLon);
    }
}
