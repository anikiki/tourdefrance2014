package com.anikiki.tdf.utils;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.anikiki.tdf.application.TdfApplication;
import com.anikiki.tdf.models.Attraction;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

/**
 * Created by Ana on 7/7/2014.
 */
public class AttractionUtils {
    private static final String TAG = AttractionUtils.class.getSimpleName();
    private static final String JSON_ROOT = "results";
    private static final String JSON_NAME = "name";
    private static final String JSON_GEOMETRY = "geometry";
    private static final String JSON_LOCATION = "location";
    private static final String JSON_LAT = "lat";
    private static final String JSON_LNG = "lng";
    private static final String SCHEME = "https";
    private static final String AUTHORITY = "maps.googleapis.com";
    private static final String PATH = "/maps/api/place/search/json";
    private static final String KEY = "key";
    private static final String RADIUS = "radius";
    private static final String RADIUS_VALUE = "5000";
    private static final String LOCATION = "location";
    private static final String TYPES = "types";
    private static final String TYPES_VALUE = "bar|cafe|campground|food|gas_station|grocery_or_supermarket|liquor_store|lodging|museum|night_club|parking|restaurant|shopping_mall|store|taxi_stand|train_station";

    public static List<Attraction> getAttractionsNearMarker(LatLng position, Context context) {
        List<Attraction> list = getAttractionsFromCache(position, context);
        if (list.isEmpty()) {
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, Utils.TIMEOUT_MILLISECONDS);
            HttpConnectionParams.setSoTimeout(httpParams, Utils.TIMEOUT_MILLISECONDS);
            HttpClient httpclient = new DefaultHttpClient();
            Uri uri = new Uri.Builder()
                    .scheme(SCHEME)
                    .authority(AUTHORITY)
                    .path(PATH)
                    .appendQueryParameter(KEY, Utils.API_KEY)
                    .appendQueryParameter(RADIUS, RADIUS_VALUE)
                    .appendQueryParameter(LOCATION, position.latitude + "," + position.longitude)
                    .appendQueryParameter(TYPES, TYPES_VALUE)
                    .build();
            HttpGet httpGet = new HttpGet(uri.toString());
            try {
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                String responseBody = httpclient.execute(httpGet, responseHandler);
                JSONObject json = new JSONObject(responseBody);
                JSONArray jArray = json.getJSONArray(JSON_ROOT);
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jObject = jArray.getJSONObject(i);
                    Attraction attraction = new Attraction();
                    attraction.setName(Utils.getStringFromJson(JSON_NAME, jObject));
                    JSONObject geometry = jObject.getJSONObject(JSON_GEOMETRY);
                    if (geometry != null) {
                        JSONObject location = geometry.getJSONObject(JSON_LOCATION);
                        if (location != null) {
                            try {
                                attraction.setPosition(new LatLng(Double.parseDouble(Utils.getStringFromJson(JSON_LAT, location)), Double.parseDouble(Utils.getStringFromJson(JSON_LNG, location))));
                            } catch (NumberFormatException e) {
                                Log.e(TAG, e.getMessage(), e);
                                attraction.setPosition(new LatLng(0d, 0d));
                            }
                        }
                    }
                    list.add(attraction);
                }
            } catch (ClientProtocolException e) {
                Log.e(TAG, e.getMessage(), e);
            } catch (IOException e) {
                Log.e(TAG, e.getMessage(), e);
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage(), e);
            }
            if (!list.isEmpty()) {
                cacheAttractions(list, position, context);
            }
        }
        return list;
    }

    private static List<Attraction> getAttractionsFromCache(LatLng position, Context context) {
        String markerId = position.hashCode() + "";
        return ((TdfApplication) context).getTdfDbHelper().readAllAttractionsForSelectedMarker(markerId);
    }

    private static void cacheAttractions(List<Attraction> attractions, LatLng position, Context context) {
        String markerId = position.hashCode() + "";
        ((TdfApplication) context).getTdfDbHelper().insertOrReplace(attractions, markerId);
    }
}
