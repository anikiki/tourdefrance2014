package com.anikiki.tdf.utils;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

/**
 * Created by Ana on 7/10/2014.
 */
public class MapUtils {
    private static final int MAP_CAMERA_UPDATE_PADDING = 125;
    private static final float MAP_CAMERA_UPDATE_ZOOM = 10f;

    public static void updateCamera(GoogleMap map, LatLngBounds.Builder boundsBuilder) {
        LatLngBounds bounds = boundsBuilder.build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, MAP_CAMERA_UPDATE_PADDING);
        map.animateCamera(cameraUpdate);
    }

    public static void updateCamera(GoogleMap map, LatLng latLng, boolean animateCamera) {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, MAP_CAMERA_UPDATE_ZOOM);
        map.animateCamera(cameraUpdate);
        if (animateCamera) {
            map.animateCamera(cameraUpdate);
        } else {
            map.moveCamera(cameraUpdate);
        }
    }
}
