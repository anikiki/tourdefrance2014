package com.anikiki.tdf.application;

import android.app.Application;
import android.support.v4.content.LocalBroadcastManager;

import com.anikiki.tdf.db.TdfDbHelper;
import com.anikiki.tdf.db.TdfDbOpenHelper;

/**
 * Created by Ana on 7/8/2014.
 */
public class TdfApplication extends Application {

    private TdfDbHelper tdfDbHelper;
    private TdfPreferences tdfPreferences;
    private LocalBroadcastManager localBroadcastManager;

    @Override
    public void onCreate() {
        super.onCreate();
        tdfDbHelper = new TdfDbHelper(new TdfDbOpenHelper(this));
        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        tdfPreferences = new TdfPreferences(this);
    }

    public TdfDbHelper getTdfDbHelper() {
        return tdfDbHelper;
    }

    public TdfPreferences getTdfPreferences() {
        return tdfPreferences;
    }

    public LocalBroadcastManager getLocalBroadcastManager() {
        return localBroadcastManager;
    }
}
