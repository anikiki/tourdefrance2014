package com.anikiki.tdf.application;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Ana on 7/28/2014.
 */
public class TdfPreferences {
    private static final String PREFS_FILE_NAME = "TdfPreferences.txt";
    private static final String START_STAGE_REMINDER_TIME = "start_stage_reminder_time";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public TdfPreferences(Context context) {
        preferences = context.getSharedPreferences(PREFS_FILE_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public long getStartStageReminderTime(int stageNumber) {
        return preferences.getLong(START_STAGE_REMINDER_TIME + stageNumber, 0);
    }

    public void setStartStageReminderTime(int stageNumber, long timeInMillis) {
        editor.putLong(START_STAGE_REMINDER_TIME + stageNumber, timeInMillis).commit();
    }
}
