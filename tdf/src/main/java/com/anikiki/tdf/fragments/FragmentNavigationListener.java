package com.anikiki.tdf.fragments;

/**
 * Created by Ana on 7/20/2014.
 */
public interface FragmentNavigationListener {
    public void handleBackNavigationFromFragment();
}
