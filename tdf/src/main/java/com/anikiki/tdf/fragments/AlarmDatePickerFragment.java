package com.anikiki.tdf.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.anikiki.tdf.R;
import com.anikiki.tdf.application.TdfApplication;
import com.anikiki.tdf.receivers.NotificationsBroadcastReceiver;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Ana on 7/27/2014.
 */
public class AlarmDatePickerFragment extends DialogFragment {
    public static final String STAGE_NAME = "stage_name";
    public static final String STAGE_NUMBER = "stage_number";
    public static final String STAGE_START = "stage_start";
    public static final String STAGE_REMINDER_TIME = "stage_reminder_time";

    private final Integer[] numbers = new Integer[]{1, 2, 3, 4, 5};
    private Spinner daysBeforeSpinner;


    public AlarmDatePickerFragment() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.msg_pick_time) + " " + getArguments().getString(STAGE_NAME))
                .setView(getCustomLayout())
                .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Calendar calendar = new GregorianCalendar();
                        calendar.setTimeInMillis(getArguments().getLong(STAGE_REMINDER_TIME));
                        calendar.add(Calendar.DAY_OF_MONTH, -(Integer) daysBeforeSpinner.getSelectedItem());
                        long alarmTimeForCurrentStage = calendar.getTimeInMillis();
                        int stageNumber = getArguments().getInt(STAGE_NUMBER);
                        ((TdfApplication) getActivity().getApplication()).getTdfPreferences().setStartStageReminderTime(stageNumber, alarmTimeForCurrentStage);

                        Intent intent = new Intent(AlarmDatePickerFragment.this.getActivity(), NotificationsBroadcastReceiver.class);
                        intent.putExtra(NotificationsBroadcastReceiver.STAGE_NAME, getArguments().getString(STAGE_NAME));
                        intent.putExtra(NotificationsBroadcastReceiver.STAGE_START, getArguments().getLong(STAGE_START));
                        AlarmDatePickerFragment.this.getActivity().sendBroadcast(intent);
                    }
                })
                .setNegativeButton(getString(android.R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }
                )
                .create();
    }

    public View getCustomLayout() {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.alarm_date_picker_fragment, null);
        daysBeforeSpinner = (Spinner) view.findViewById(R.id.days_before_id);
        ArrayAdapter<Integer> spinnerAdapter = new ArrayAdapter<Integer>(getActivity(), android.R.layout.simple_spinner_item, numbers);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        daysBeforeSpinner.setAdapter(spinnerAdapter);
        return view;
    }
}
