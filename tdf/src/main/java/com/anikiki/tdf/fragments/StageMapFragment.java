package com.anikiki.tdf.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.anikiki.tdf.R;
import com.anikiki.tdf.application.TdfApplication;
import com.anikiki.tdf.helpers.MapMarkerHelper;
import com.anikiki.tdf.location.TdfLocationClient;
import com.anikiki.tdf.models.Attraction;
import com.anikiki.tdf.models.Hill;
import com.anikiki.tdf.models.SpectatorHub;
import com.anikiki.tdf.models.Sprint;
import com.anikiki.tdf.models.Stage;
import com.anikiki.tdf.receivers.AttractionsBroadcastReceiver;
import com.anikiki.tdf.utils.AttractionUtils;
import com.anikiki.tdf.utils.FileUtils;
import com.anikiki.tdf.utils.MapUtils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Ana on 7/5/2014.
 */
public class StageMapFragment extends MapFragment implements HandleBackKey {
    private static final String TAG = StageMapFragment.class.getSimpleName();
    private static final String EVENT_SEND_ATTRACTIONS = "event_get_attractions";
    private static final String SAVED_SPECTATOR_HUBS = "saved_spectator_hubs";
    private static final String SAVED_PASSES = "saved_passes";
    private static final String SAVED_SPRINTS = "saved_sprints";

    public static final String ROUTE_FILE_ID = "route_file_id";
    public static final String HUB_MARKER = "hub_marker";
    public static final String HUB_MARKER_POSITION = "hub_marker_position";
    public static final String STAGE_NUMBER = "stage_number";
    public static final String ANIMATE_CAMERA = "animate_camera";

    private boolean routeDisplayed = false;
    private boolean hubsDisplayed = false;
    private boolean passesSprintsDisplayed = false;
    private DrawRouteTask drawRouteTask;
    private StageMapListener stageMapListener;
    private MapMarkerHelper mapMarkerHelper;
    private List<Marker> spectatorHubMarkers;
    private List<Marker> passesSprintsMarkers;
    private Stage savedStage;
    private GoogleMap map;
    private LocalBroadcastManager localBroadcastManager;
    private AttractionsBroadcastReceiver attractionsBroadcastReceiver;
    private TdfLocationClient tdfLocationClient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedStage == null) {
            savedStage = new Stage();
        }
        if (savedInstanceState != null) {
            readSavedState(savedInstanceState);
        }
        setHasOptionsMenu(true);
        localBroadcastManager = ((TdfApplication) getActivity().getApplication()).getLocalBroadcastManager();
        attractionsBroadcastReceiver = new AttractionsBroadcastReceiver();
        localBroadcastManager.registerReceiver(attractionsBroadcastReceiver, new IntentFilter(EVENT_SEND_ATTRACTIONS));
        tdfLocationClient = new TdfLocationClient(getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();
        tdfLocationClient.connect();
    }

    @Override
    public void onStop() {
        tdfLocationClient.disconnect();
        super.onStop();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mapMarkerHelper = new MapMarkerHelper();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.map_marker_menu, menu);
        menu.findItem(R.id.navigate_menu).setVisible(mapMarkerHelper.isMarkerClicked());
        if ((spectatorHubMarkers != null && spectatorHubMarkers.contains(mapMarkerHelper.getSelectedMarker())) ||
                (passesSprintsMarkers != null && passesSprintsMarkers.contains(mapMarkerHelper.getSelectedMarker()))) {
            menu.findItem(R.id.attractions_menu).setVisible(mapMarkerHelper.isMarkerClicked());
        } else {
            menu.findItem(R.id.attractions_menu).setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigate_menu:
                mapMarkerHelper.startNavigation(getActivity(), tdfLocationClient.getLastReadLocation());
                return true;
            case R.id.attractions_menu:
                findAttractions();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void findAttractions() {
        if (mapMarkerHelper.getSelectedMarker() != null) {
            new AttractionsNearMarker().execute(mapMarkerHelper.getSelectedMarker().getPosition());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (map == null) {
            map = getMap();
        }
        map.getUiSettings().setCompassEnabled(false);
        populateAllMarkers();
        if (!routeDisplayed) {
            routeDisplayed = true;
            drawRoute();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (hubsDisplayed) {
            saveState(outState, savedStage.getSpectatorHubs(), SAVED_SPECTATOR_HUBS);
        }
        if (passesSprintsDisplayed) {
            saveState(outState, savedStage.getHills(), SAVED_PASSES);
            saveState(outState, savedStage.getSprints(), SAVED_SPRINTS);
        }
        super.onSaveInstanceState(outState);
    }

    private void saveState(Bundle outState, List<? extends Parcelable> parcelables, String tag) {
        if (parcelables != null) {
            int pos = 0;
            for (Parcelable parcelable : parcelables) {
                Parcel parcel = Parcel.obtain();
                parcelable.writeToParcel(parcel, 0);
                parcel.setDataPosition(0);
                outState.putByteArray(tag + pos, parcel.marshall());
                pos++;
            }
        }
    }

    private void readSavedState(Bundle savedInstanceState) {
        Set<String> keys = savedInstanceState.keySet();
        List<SpectatorHub> hubs = new ArrayList<SpectatorHub>();
        List<Hill> hills = new ArrayList<Hill>();
        List<Sprint> sprints = new ArrayList<Sprint>();
        for (String key : keys) {
            if (key.startsWith(SAVED_SPECTATOR_HUBS) || key.startsWith(SAVED_PASSES) || key.startsWith(SAVED_SPRINTS)) {
                byte[] info = savedInstanceState.getByteArray(key);
                Parcel parcel = Parcel.obtain();
                parcel.unmarshall(info, 0, info.length);
                parcel.setDataPosition(0);
                if (key.startsWith(SAVED_SPECTATOR_HUBS)) {
                    SpectatorHub data = SpectatorHub.CREATOR.createFromParcel(parcel);
                    hubs.add(data);
                } else if (key.startsWith(SAVED_PASSES)) {
                    Hill data = Hill.CREATOR.createFromParcel(parcel);
                    hills.add(data);
                } else if (key.startsWith(SAVED_SPRINTS)) {
                    Sprint data = Sprint.CREATOR.createFromParcel(parcel);
                    sprints.add(data);
                }
            }
        }
        savedStage.setSpectatorHubs(hubs);
        savedStage.setHills(hills);
        savedStage.setSprints(sprints);
    }

    @Override
    public void onDestroy() {
        localBroadcastManager.unregisterReceiver(attractionsBroadcastReceiver);
        super.onDestroy();
    }

    @Override
    public void onPause() {
        cancelDrawRoute();
        super.onPause();
    }

    public void setListener(StageMapListener stageMapListener) {
        this.stageMapListener = stageMapListener;
    }

    public void setHubsDisplayed(boolean hubsDisplayed) {
        this.hubsDisplayed = hubsDisplayed;
    }

    public void setPassesSprintsDisplayed(boolean passesSprintsDisplayed) {
        this.passesSprintsDisplayed = passesSprintsDisplayed;
    }

    private void setMapListeners() {
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                int position = -1;
                if (spectatorHubMarkers != null) {
                    position = spectatorHubMarkers.indexOf(marker);
                    if (position == 0 && spectatorHubMarkers.size() == 1) {
                        position = getArguments().getInt(HUB_MARKER_POSITION);
                    }
                }
                if (position >= 0 && stageMapListener != null) {
                    stageMapListener.onSelectStageSpectatorHub(position);
                }
            }
        });
    }

    private void drawRoute() {
        if (drawRouteTask == null || drawRouteTask.getStatus() == DrawRouteTask.Status.FINISHED) {
            SpectatorHub hub = getArguments().getParcelable(HUB_MARKER);
            if (hub != null && map != null) {
                spectatorHubMarkers = new ArrayList<Marker>();
                spectatorHubMarkers.add(map.addMarker(new MarkerOptions()
                        .position(hub.getLatLong())
                        .title(hub.getName() + " - " + hub.getCapacity())
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW))));
                mapMarkerHelper.setMapMarkerListeners(map, getActivity());
                MapUtils.updateCamera(map, hub.getLatLong(), getArguments().getBoolean(ANIMATE_CAMERA));
            }
            drawRouteTask = (DrawRouteTask) new DrawRouteTask().execute();
        } else {
            Toast.makeText(getActivity(), R.string.err_load_route, Toast.LENGTH_LONG).show();
        }
    }

    private void cancelDrawRoute() {
        if (drawRouteTask != null && drawRouteTask.getStatus() == DrawRouteTask.Status.RUNNING) {
            getActivity().setProgressBarIndeterminateVisibility(Boolean.FALSE);
            drawRouteTask.cancel(true);
            drawRouteTask = null;
        }
    }

    @Override
    public boolean handleBackKey() {
        int position = getArguments().getInt(HUB_MARKER_POSITION, -1);
        if (stageMapListener != null && position >= 0) {
            stageMapListener.onSelectStageSpectatorHub(position);
            return true;
        }
        return false;
    }

    private class DrawRouteTask extends AsyncTask<Void, Void, List<LatLng>> {
        @Override
        protected void onPreExecute() {
            getActivity().setProgressBarIndeterminateVisibility(Boolean.TRUE);
        }

        @Override
        protected List<LatLng> doInBackground(Void... params) {
            return FileUtils.getPointsFromFile(getArguments().getInt(ROUTE_FILE_ID), getActivity());
        }

        @Override
        protected void onPostExecute(final List<LatLng> list) {
            if (map == null) {
                map = getMap();
            }
            if (map != null) {
                LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
                for (LatLng ll : list) {
                    boundsBuilder.include(ll);
                }
                map.addPolyline(new PolylineOptions()
                        .addAll(list)
                        .width(5)
                        .color(Color.RED));
                map.addMarker(new MarkerOptions()
                        .position(list.get(0))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_start_flag)));
                map.addMarker(new MarkerOptions()
                        .position(list.get(list.size() - 1))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_finish_flag)));
                SpectatorHub hub = getArguments().getParcelable(HUB_MARKER);
                if (hub == null && savedStage.getSpectatorHubs() == null && savedStage.getHills() == null && savedStage.getSprints() == null) {
                    MapUtils.updateCamera(map, boundsBuilder);
                }
                setMapListeners();
            }
            getActivity().setProgressBarIndeterminateVisibility(Boolean.FALSE);
        }
    }

    public void displayPassesSprints(boolean passesSprintsChecked) {
        if (passesSprintsMarkers != null && passesSprintsMarkers.size() > 0) {
            showHideMarkers(passesSprintsChecked, passesSprintsMarkers);
        } else {
            new DrawPassesSprintsTask().execute(getArguments().getInt(STAGE_NUMBER));
        }
    }

    public void displaySpectatorHubs(boolean hubsChecked) {
        if (spectatorHubMarkers != null && spectatorHubMarkers.size() > 1) {
            showHideMarkers(hubsChecked, spectatorHubMarkers);
        } else {
            new DrawSpectatorHubsTask().execute(getArguments().getInt(STAGE_NUMBER));
        }
    }

    private void populateAllMarkers() {
        if (savedStage == null) {
            return;
        }
        populateSpectatorHubs(savedStage, map);
        populatePassesSprints(savedStage, map);
    }

    private void showHideMarkers(boolean display, List<Marker> markers) {
        if (markers != null && markers.size() > 0) {
            if (display) {
                LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
                for (Marker marker : markers) {
                    marker.setVisible(true);
                    boundsBuilder.include(marker.getPosition());
                }
                MapUtils.updateCamera(map, boundsBuilder);
            } else {
                for (Marker marker : markers) {
                    marker.setVisible(false);
                }
            }
        }
    }

    private void populateSpectatorHubs(Stage stage, GoogleMap map) {
        List<SpectatorHub> hubs = stage.getSpectatorHubs();
        if (hubs != null) {
            spectatorHubMarkers = new ArrayList<Marker>();
            LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
            for (SpectatorHub hub : hubs) {
                MarkerOptions markerOptions = new MarkerOptions()
                        .position(hub.getLatLong())
                        .title(hub.getName() + " - " + hub.getCapacity())
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
                spectatorHubMarkers.add(map.addMarker(markerOptions));
                boundsBuilder.include(hub.getLatLong());
            }
            //         MapUtils.updateCamera(map, boundsBuilder);
            mapMarkerHelper.setMapMarkerListeners(map, getActivity());
        }
    }

    private void populatePassesSprints(Stage stage, GoogleMap map) {
        List<Hill> passes = stage.getHills();
        List<Sprint> sprints = stage.getSprints();
        if (passes != null || sprints != null) {
            passesSprintsMarkers = new ArrayList<Marker>();
            LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
            if (passes != null) {
                for (Hill pass : passes) {
                    MarkerOptions marker = new MarkerOptions()
                            .position(pass.getLatLong())
                            .title(pass.getName())
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                    passesSprintsMarkers.add(map.addMarker(marker));
                    boundsBuilder.include(pass.getLatLong());
                }
            }
            if (sprints != null) {
                for (Sprint sprint : sprints) {
                    MarkerOptions marker = new MarkerOptions()
                            .position(sprint.getLatLong())
                            .title(sprint.getName())
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
                    passesSprintsMarkers.add(map.addMarker(marker));
                    boundsBuilder.include(sprint.getLatLong());
                }
            }
            //       MapUtils.updateCamera(map, boundsBuilder);
            mapMarkerHelper.setMapMarkerListeners(map, getActivity());
        }
    }

    private class DrawSpectatorHubsTask extends AsyncTask<Integer, Void, Stage> {
        @Override
        protected Stage doInBackground(Integer... params) {
            Stage stage = null;
            JSONObject obj = FileUtils.convertFile(R.raw.stages, getActivity());
            if (obj != null) {
                stage = FileUtils.getSpectatorHubsForStage(obj, params[0]);
            }
            return stage;
        }

        @Override
        protected void onPostExecute(Stage stage) {
            if (map == null) {
                map = getMap();
            }
            if (map != null) {
                savedStage.setSpectatorHubs(stage.getSpectatorHubs());
                populateSpectatorHubs(stage, map);
            }
        }
    }

    private class DrawPassesSprintsTask extends AsyncTask<Integer, Void, Stage> {
        @Override
        protected Stage doInBackground(Integer... params) {
            Stage stage = null;
            JSONObject obj = FileUtils.convertFile(R.raw.stages, getActivity());
            if (obj != null) {
                stage = FileUtils.getPassesAndSprintsForStage(obj, params[0]);
            }
            return stage;
        }

        @Override
        protected void onPostExecute(Stage stage) {
            if (map == null) {
                map = getMap();
            }
            if (map != null) {
                savedStage.setHills(stage.getHills());
                savedStage.setSprints(stage.getSprints());
                populatePassesSprints(stage, map);
            }
        }
    }

    private class AttractionsNearMarker extends AsyncTask<LatLng, Void, Void> {
        @Override
        protected Void doInBackground(LatLng... params) {
            ArrayList<Attraction> attractions = (ArrayList<Attraction>) AttractionUtils.getAttractionsNearMarker(params[0], getActivity().getApplication());
            Intent intent = new Intent(EVENT_SEND_ATTRACTIONS);
            intent.putParcelableArrayListExtra(AttractionsBroadcastReceiver.ATTRACTIONS_LIST, attractions);
            if (map == null) {
                map = getMap();
            }
            attractionsBroadcastReceiver.setMap(map);
            localBroadcastManager.sendBroadcast(intent);
            return null;
        }
    }

    public interface StageMapListener {
        public void onSelectStageSpectatorHub(int position);
    }
}