package com.anikiki.tdf.fragments;

/**
 * Created by Ana on 7/10/2014.
 */
public interface HandleBackKey {
    public boolean handleBackKey();
}
