package com.anikiki.tdf.fragments;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.anikiki.tdf.R;
import com.anikiki.tdf.adapters.PassesArrayAdapter;
import com.anikiki.tdf.adapters.SprintsArrayAdapter;
import com.anikiki.tdf.models.Hill;
import com.anikiki.tdf.models.Sprint;
import com.anikiki.tdf.models.Stage;
import com.anikiki.tdf.utils.FileUtils;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by Ana on 7/6/2014.
 */
public class StagePassesAndSprintsFragment extends Fragment implements HandleBackKey {
    private static final String TAG = StagePassesAndSprintsFragment.class.getSimpleName();
    public static final String STAGE_NUMBER = "stage_number";

    private View rootView;
    private LoadPassesSprintsForStageTask loadPassesSprintsForStageTask;
    private FragmentNavigationListener fragmentNavigationListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().invalidateOptionsMenu();
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.passes_sprints_fragment, container, false);
        loadPassesSprintsForStage();
        return rootView;
    }

    @Override
    public void onDestroyView() {
        cancelLoadPassesSprintsForStage();
        super.onDestroyView();
    }

    @Override
    public boolean handleBackKey() {
        if (fragmentNavigationListener != null) {
            fragmentNavigationListener.handleBackNavigationFromFragment();
            return true;
        }
        return false;
    }

    public void setListener(FragmentNavigationListener fragmentNavigationListener) {
        this.fragmentNavigationListener = fragmentNavigationListener;
    }

    private void loadPassesSprintsForStage() {
        if (loadPassesSprintsForStageTask == null || loadPassesSprintsForStageTask.getStatus() == LoadPassesSprintsForStageTask.Status.FINISHED) {
            loadPassesSprintsForStageTask = (LoadPassesSprintsForStageTask) new LoadPassesSprintsForStageTask().execute(getArguments().getInt(STAGE_NUMBER));
        } else {
            Toast.makeText(getActivity(), R.string.err_load_passes_sprints, Toast.LENGTH_LONG).show();
        }
    }

    private void cancelLoadPassesSprintsForStage() {
        if (loadPassesSprintsForStageTask != null && loadPassesSprintsForStageTask.getStatus() == LoadPassesSprintsForStageTask.Status.RUNNING) {
            getActivity().setProgressBarIndeterminateVisibility(false);
            loadPassesSprintsForStageTask.cancel(true);
            loadPassesSprintsForStageTask = null;
        }
    }

    private class LoadPassesSprintsForStageTask extends AsyncTask<Integer, Void, Stage> {
        @Override
        protected Stage doInBackground(Integer... params) {
            Stage stage = null;
            JSONObject obj = FileUtils.convertFile(R.raw.stages, getActivity());
            if (obj != null) {
                stage = FileUtils.getPassesAndSprintsForStage(obj, params[0]);
            }
            return stage;
        }

        @Override
        protected void onPostExecute(Stage stage) {
            super.onPostExecute(stage);
            List<Hill> hills = stage.getHills();
            List<Sprint> sprints = stage.getSprints();
            PassesArrayAdapter passesAdapter = new PassesArrayAdapter(getActivity(), hills);
            SprintsArrayAdapter sprintsAdapter = new SprintsArrayAdapter(getActivity(), sprints);
            ((ListView) rootView.findViewById(R.id.passes)).setAdapter(passesAdapter);
            ((ListView) rootView.findViewById(R.id.sprints)).setAdapter(sprintsAdapter);
            if (hills == null || hills.size() == 0) {
                rootView.findViewById(R.id.no_passes).setVisibility(View.VISIBLE);
            }
            if (sprints == null || sprints.size() == 0) {
                rootView.findViewById(R.id.no_sprints).setVisibility(View.VISIBLE);
            }
        }
    }
}
