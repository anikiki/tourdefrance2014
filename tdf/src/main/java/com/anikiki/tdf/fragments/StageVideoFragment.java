package com.anikiki.tdf.fragments;

import android.os.Bundle;

import com.anikiki.tdf.utils.Utils;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;

/**
 * Created by Ana on 7/6/2014.
 */
public class StageVideoFragment extends YouTubePlayerFragment {
    private static final String TAG = StageVideoFragment.class.getSimpleName();
    public static final String STAGE_NUMBER = "stage_number";
    public static final String VIDEO_ID = "video_id";

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getActivity().invalidateOptionsMenu();
        setRetainInstance(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        initialize(Utils.API_KEY, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
                if (!wasRestored) {
                    youTubePlayer.cueVideo(getArguments().getString(VIDEO_ID));
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
            }
        });
    }
}
