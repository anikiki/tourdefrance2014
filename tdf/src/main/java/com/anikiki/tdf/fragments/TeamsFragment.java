package com.anikiki.tdf.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.anikiki.tdf.R;
import com.anikiki.tdf.adapters.TeamsArrayAdapter;
import com.anikiki.tdf.adapters.items.ListItem;
import com.anikiki.tdf.utils.FileUtils;
import com.anikiki.tdf.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ana on 7/4/2014.
 */
public class TeamsFragment extends Fragment {
    private static final String TAG = TeamsFragment.class.getSimpleName();
    private ListView teamsView;
    private TeamSelectedListener teamSelectedListener;
    private LoadTeamsAndRidersTask teamsAndRidersTask;
    private View lastSelectedView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().invalidateOptionsMenu();
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadTeamsAndRiders();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.list_teams_fragment, container, false);
        teamsView = (ListView) rootView.findViewById(R.id.teams_list);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            teamSelectedListener = (TeamSelectedListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        invalidateLastSelectedView();
    }

    @Override
    public void onPause() {
        cancelLoadTeamsAndRiders();
        super.onPause();
    }

    public interface TeamSelectedListener {
        public void onTeamSelected(int position, List<ListItem> teams);
    }

    private void loadTeamsAndRiders() {
        if (teamsAndRidersTask == null || teamsAndRidersTask.getStatus() == LoadTeamsAndRidersTask.Status.FINISHED) {
            teamsAndRidersTask = (LoadTeamsAndRidersTask) new LoadTeamsAndRidersTask().execute();
        } else {
            Toast.makeText(getActivity(), R.string.err_load_teams_riders, Toast.LENGTH_LONG).show();
        }
    }

    private void cancelLoadTeamsAndRiders() {
        if (teamsAndRidersTask != null && teamsAndRidersTask.getStatus() == LoadTeamsAndRidersTask.Status.RUNNING) {
            teamsAndRidersTask.cancel(true);
            teamsAndRidersTask = null;
        }
    }

    private void invalidateLastSelectedView() {
        if (lastSelectedView != null) {
            lastSelectedView.setSelected(false);
        }
    }

    private class LoadTeamsAndRidersTask extends AsyncTask<Void, Void, List<ListItem>> {
        @Override
        protected List<ListItem> doInBackground(Void... params) {
            List<ListItem> teams = new ArrayList<ListItem>();
            JSONObject obj = FileUtils.convertFile(R.raw.teams, getActivity());
            if (obj != null) {
                teams = FileUtils.getTeams(obj, getActivity());
            }
            return teams;
        }

        @Override
        protected void onPostExecute(final List<ListItem> teams) {
            super.onPostExecute(teams);
            TeamsArrayAdapter adapter = new TeamsArrayAdapter(getActivity(), teams);
            teamsView.setAdapter(adapter);
            setFirstTeamSelected(teams);
            teamsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    invalidateLastSelectedView();
                    teamSelectedListener.onTeamSelected(i, teams);
                    if (Utils.isLargeScreen(getActivity()) && getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                        view.setSelected(true);
                        lastSelectedView = view;
                    }
                }
            });
            adapter.notifyDataSetChanged();
        }

        private void setFirstTeamSelected(List<ListItem> teams) {
            if (Utils.isLargeScreen(getActivity()) && getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                teamsView.setChoiceMode(1);
                teamsView.setItemChecked(0, true);
                teamSelectedListener.onTeamSelected(0, teams);
                lastSelectedView = teamsView.getSelectedView();
            }
        }
    }
}