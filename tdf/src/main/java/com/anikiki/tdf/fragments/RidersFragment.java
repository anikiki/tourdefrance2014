package com.anikiki.tdf.fragments;

import android.app.ListFragment;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anikiki.tdf.R;
import com.anikiki.tdf.adapters.RidersArrayAdapter;
import com.anikiki.tdf.adapters.items.ListItem;
import com.anikiki.tdf.models.Rider;
import com.anikiki.tdf.models.Team;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ana on 7/4/2014.
 */
public class RidersFragment extends ListFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().invalidateOptionsMenu();
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_riders_fragment, container, false);
    }

    public void updateRidersDisplay(int position, List<ListItem> teams) {
        List<Rider> riders = ((Team) teams.get(position).getItem()).getRiders();
        RidersArrayAdapter adapter = new RidersArrayAdapter(getActivity(), riders);
        setListAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    /**
     * This method is used to populate the list of riders for small to normal screens.
     * <p/>
     * {@link com.anikiki.tdf.fragments.RidersFragment} is used alone in an activity
     * for small to normal screens and along with {@link com.anikiki.tdf.fragments.TeamsFragment}
     * in another activity for large screens. Updates to the content of this fragment can be made
     * after the fragment was created.
     *
     * @param riders List of riders to be displayed.
     */
    public void updateRidersDisplay(ArrayList<? extends Parcelable> riders) {
        RidersArrayAdapter adapter = new RidersArrayAdapter(getActivity(), (ArrayList<Rider>) riders);
        setListAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
