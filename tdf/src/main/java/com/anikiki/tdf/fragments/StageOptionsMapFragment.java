package com.anikiki.tdf.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.anikiki.tdf.R;

/**
 * Created by Ana on 7/6/2014.
 */
public class StageOptionsMapFragment extends Fragment implements HandleBackKey {
    private static final String TAG = StageOptionsMapFragment.class.getSimpleName();
    private static final String PASSES_SPRINTS_CHECKED = "passes_sprints_checked";
    private static final String HUBS_CHECKED = "hubs_checked";
    private static final String MAP_FRAGMENT_TAG = "map_fragment_tag";

    public static final String ROUTE_FILE_ID = StageMapFragment.ROUTE_FILE_ID;
    public static final String HUB_MARKER = StageMapFragment.HUB_MARKER;
    public static final String HUB_MARKER_POSITION = StageMapFragment.HUB_MARKER_POSITION;
    public static final String STAGE_NUMBER = StageMapFragment.STAGE_NUMBER;
    public static final String ANIMATE_CAMERA = StageMapFragment.ANIMATE_CAMERA;

    private ImageButton passesSprintsButton;
    private ImageButton hubsButton;
    private StageMapFragment mapFragment;
    private StageOptionsMapListener stageOptionsMapListener;
    private FragmentNavigationListener fragmentNavigationListener;
    private boolean passesSprintsChecked;
    private boolean hubsChecked;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            passesSprintsChecked = savedInstanceState.getBoolean(PASSES_SPRINTS_CHECKED);
            hubsChecked = savedInstanceState.getBoolean(HUBS_CHECKED);
        } else {
            passesSprintsChecked = false;
            hubsChecked = false;
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mapFragment = (StageMapFragment) getChildFragmentManager().findFragmentByTag(MAP_FRAGMENT_TAG);
        if (mapFragment == null) {
            mapFragment = new StageMapFragment();
            mapFragment.setArguments(getArguments());
            mapFragment.setListener(stageOptionsMapListener);
            getChildFragmentManager().beginTransaction().replace(R.id.map_container, mapFragment, MAP_FRAGMENT_TAG).commit();
        } else {
            mapFragment.setHubsDisplayed(hubsChecked);
            mapFragment.setPassesSprintsDisplayed(passesSprintsChecked);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.map_and_checkboxes_fragment, container, false);
        passesSprintsButton = (ImageButton) rootView.findViewById(R.id.map_passes_sprints_button);
        hubsButton = (ImageButton) rootView.findViewById(R.id.map_hubs_button);
        optionChecked(passesSprintsChecked, R.drawable.ic_passes_on, R.drawable.ic_passes_off, passesSprintsButton);
        optionChecked(hubsChecked, R.drawable.ic_spectator_hub_on, R.drawable.ic_spectator_hub_off, hubsButton);
        setListeners();
        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(PASSES_SPRINTS_CHECKED, passesSprintsChecked);
        outState.putBoolean(HUBS_CHECKED, hubsChecked);
    }

    @Override
    public boolean handleBackKey() {
        int position = getArguments().getInt(HUB_MARKER_POSITION, -1);
        if (stageOptionsMapListener != null && position >= 0) {
            stageOptionsMapListener.onSelectStageSpectatorHub(position);
            return true;
        } else if (fragmentNavigationListener != null) {
            fragmentNavigationListener.handleBackNavigationFromFragment();
            return true;
        }
        return false;
    }

    public StageOptionsMapFragment setListener(StageOptionsMapListener stageOptionsMapListener) {
        this.stageOptionsMapListener = stageOptionsMapListener;
        return this;
    }

    public StageOptionsMapFragment setListener(FragmentNavigationListener fragmentNavigationListener) {
        this.fragmentNavigationListener = fragmentNavigationListener;
        return this;
    }

    private void setListeners() {
        passesSprintsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                passesSprintsChecked = !passesSprintsChecked;
                mapFragment.setPassesSprintsDisplayed(passesSprintsChecked);
                mapFragment.displayPassesSprints(optionChecked(passesSprintsChecked, R.drawable.ic_passes_on, R.drawable.ic_passes_off, v));
            }
        });

        hubsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hubsChecked = !hubsChecked;
                mapFragment.setHubsDisplayed(hubsChecked);
                mapFragment.displaySpectatorHubs(optionChecked(hubsChecked, R.drawable.ic_spectator_hub_on, R.drawable.ic_spectator_hub_off, v));
            }
        });
    }

    private boolean optionChecked(boolean checked, int idImageOn, int idImageOff, View view) {
        if (checked) {
            view.setBackground(getActivity().getResources().getDrawable(idImageOn));
        } else {
            view.setBackground(getActivity().getResources().getDrawable(idImageOff));
        }
        return checked;
    }

    public interface StageOptionsMapListener extends StageMapFragment.StageMapListener {
    }
}
