package com.anikiki.tdf.fragments;

import android.app.Fragment;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidplot.LineRegion;
import com.androidplot.ui.AnchorPosition;
import com.androidplot.ui.SeriesRenderer;
import com.androidplot.ui.SizeLayoutType;
import com.androidplot.ui.SizeMetrics;
import com.androidplot.ui.TextOrientationType;
import com.androidplot.ui.XLayoutStyle;
import com.androidplot.ui.YLayoutStyle;
import com.androidplot.ui.widget.TextLabelWidget;
import com.androidplot.util.PixelUtils;
import com.androidplot.xy.BarFormatter;
import com.androidplot.xy.BarRenderer;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.anikiki.tdf.R;
import com.anikiki.tdf.utils.FileUtils;

import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Ana on 7/6/2014.
 */
public class StageProfileFragment extends Fragment implements HandleBackKey {
    private static final String TAG = StageProfileFragment.class.getSimpleName();
    private static final String NO_SELECTION_TXT = "Touch bar to select.";
    private static final int BAR_WIDTH = 10;
    private static final int BAR_GAP_WIDTH = 5;

    public static final String STAGE_NUMBER = "stage_number";
    public static final String ROUTE_FILE_ID = "route_file_id";

    private XYPlot plot;
    private CustomBarFormatter formatter;
    private CustomBarFormatter selectionFormatter;
    private TextLabelWidget selectionWidget;
    private Pair<Integer, XYSeries> selection;
    private DrawElevationProfileForStage drawElevationProfileForStage;
    private FragmentNavigationListener fragmentNavigationListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().invalidateOptionsMenu();
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.stage_profile_fragment, container, false);
        // initialize our XYPlot reference
        plot = (XYPlot) rootView.findViewById(R.id.mySimpleXYPlot);
        drawElevationProfile();
        return rootView;
    }

    @Override
    public void onDestroyView() {
        cancelDrawElevationProfile();
        super.onDestroyView();
    }

    @Override
    public boolean handleBackKey() {
        if (fragmentNavigationListener != null) {
            fragmentNavigationListener.handleBackNavigationFromFragment();
            return true;
        }
        return false;
    }

    public void setListener(FragmentNavigationListener fragmentNavigationListener) {
        this.fragmentNavigationListener = fragmentNavigationListener;
    }

    private void drawElevationProfile() {
        if (drawElevationProfileForStage == null || drawElevationProfileForStage.getStatus() == DrawElevationProfileForStage.Status.FINISHED) {
            drawElevationProfileForStage = (DrawElevationProfileForStage) new DrawElevationProfileForStage().execute();
        } else {
            Toast.makeText(getActivity(), R.string.err_load_profile, Toast.LENGTH_LONG).show();
        }
    }

    private void cancelDrawElevationProfile() {
        if (drawElevationProfileForStage != null && drawElevationProfileForStage.getStatus() == DrawElevationProfileForStage.Status.RUNNING) {
            getActivity().setProgressBarIndeterminateVisibility(false);
            drawElevationProfileForStage.cancel(true);
            drawElevationProfileForStage = null;
        }
    }

    private void updatePlot(List<Number> series1Numbers) {
        // remove all current series from each plot
        Iterator<XYSeries> iterator1 = plot.getSeriesSet().iterator();
        while (iterator1.hasNext()) {
            XYSeries setElement = iterator1.next();
            plot.removeSeries(setElement);
        }
        // setup the Series with the selected number of elements
        XYSeries series = new SimpleXYSeries(series1Numbers, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.title_elevation));
        // add the series to the xyplot
        plot.addSeries(series, formatter);
        // setup the BarRenderer with selected options
        CustomBarRenderer renderer = ((CustomBarRenderer) plot.getRenderer(CustomBarRenderer.class));
        renderer.setBarRenderStyle(BarRenderer.BarRenderStyle.STACKED);
        renderer.setBarWidthStyle(BarRenderer.BarWidthStyle.FIXED_WIDTH);
        renderer.setBarWidth(BAR_WIDTH);
        renderer.setBarGap(BAR_GAP_WIDTH);
        plot.setRangeTopMin(15);
        plot.redraw();
    }

    private void onPlotClicked(PointF point) {
        // make sure the point lies within the graph area
        if (plot.getGraphWidget().getGridRect().contains(point.x, point.y)) {
            Number x = plot.getXVal(point);
            Number y = plot.getYVal(point);

            selection = null;
            double xDistance = 0;
            double yDistance = 0;

            // find the closest value to the selection
            for (XYSeries xySeries : plot.getSeriesSet()) {
                for (int i = 0; i < xySeries.size(); i++) {
                    Number thisX = xySeries.getX(i);
                    Number thisY = xySeries.getY(i);
                    if (thisX != null && thisY != null) {
                        double thisXDistance =
                                LineRegion.measure(x, thisX).doubleValue();
                        double thisYDistance =
                                LineRegion.measure(y, thisY).doubleValue();
                        if (selection == null) {
                            selection = new Pair<Integer, XYSeries>(i, xySeries);
                            xDistance = thisXDistance;
                            yDistance = thisYDistance;
                        } else if (thisXDistance < xDistance) {
                            selection = new Pair<Integer, XYSeries>(i, xySeries);
                            xDistance = thisXDistance;
                            yDistance = thisYDistance;
                        } else if (thisXDistance == xDistance &&
                                thisYDistance < yDistance &&
                                thisY.doubleValue() >= y.doubleValue()) {
                            selection = new Pair<Integer, XYSeries>(i, xySeries);
                            xDistance = thisXDistance;
                            yDistance = thisYDistance;
                        }
                    }
                }
            }

        } else {
            // if the press was outside the graph area, deselect
            selection = null;
        }

        if (selection == null) {
            selectionWidget.setText(NO_SELECTION_TXT);
        } else {
            selectionWidget.setText("Selected: " + selection.second.getTitle() + " Value: " + selection.second.getY(selection.first));
        }
        plot.redraw();
    }

    class CustomBarFormatter extends BarFormatter {
        public CustomBarFormatter(int fillColor, int borderColor) {
            super(fillColor, borderColor);
        }

        @Override
        public Class<? extends SeriesRenderer> getRendererClass() {
            return CustomBarRenderer.class;
        }

        @Override
        public SeriesRenderer getRendererInstance(XYPlot plot) {
            return new CustomBarRenderer(plot);
        }
    }

    class CustomBarRenderer extends BarRenderer<CustomBarFormatter> {
        public CustomBarRenderer(XYPlot plot) {
            super(plot);
        }

        @Override
        public CustomBarFormatter getFormatter(int index, XYSeries xySeries) {
            // inject special selection formatter
            if (selection != null && selection.second == xySeries && selection.first == index) {
                return selectionFormatter;
            } else {
                return getFormatter(xySeries);
            }
        }
    }

    private class DrawElevationProfileForStage extends AsyncTask<Void, Void, List<Number>> {
        @Override
        protected void onPreExecute() {
            getActivity().setProgressBarIndeterminateVisibility(Boolean.TRUE);
        }

        @Override
        protected List<Number> doInBackground(Void... params) {
            return FileUtils.getElevationPointsFromFile(getArguments().getInt(ROUTE_FILE_ID), getActivity());
        }

        @Override
        protected void onPostExecute(List<Number> numbers) {
            formatter = new CustomBarFormatter(Color.argb(200, 100, 150, 100), Color.LTGRAY);
            selectionFormatter = new CustomBarFormatter(Color.YELLOW, Color.WHITE);
            selectionWidget = new TextLabelWidget(plot.getLayoutManager(), NO_SELECTION_TXT,
                    new SizeMetrics(
                            PixelUtils.dpToPix(100), SizeLayoutType.ABSOLUTE,
                            PixelUtils.dpToPix(100), SizeLayoutType.ABSOLUTE),
                    TextOrientationType.HORIZONTAL
            );

            selectionWidget.getLabelPaint().setTextSize(PixelUtils.dpToPix(16));

            // add a dark, semi-transparent background to the selection label widget
            Paint p = new Paint();
            p.setARGB(100, 0, 0, 0);
            selectionWidget.setBackgroundPaint(p);
            selectionWidget.position(
                    PixelUtils.dpToPix(145), XLayoutStyle.ABSOLUTE_FROM_LEFT,
                    PixelUtils.dpToPix(45), YLayoutStyle.ABSOLUTE_FROM_TOP,
                    AnchorPosition.TOP_MIDDLE);
            selectionWidget.pack();

            // reduce the number of range labels
            plot.setTicksPerRangeLabel(3);
            plot.setRangeLowerBoundary(0, BoundaryMode.FIXED);
            plot.getGraphWidget().setGridPadding(30, 30, 30, 0);
            plot.setTicksPerDomainLabel(2);
            plot.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                        onPlotClicked(new PointF(motionEvent.getX(), motionEvent.getY()));
                    }
                    return true;
                }
            });

            plot.setDomainValueFormat(new NumberFormat() {
                @Override
                public StringBuffer format(double value, StringBuffer buffer, FieldPosition field) {
                    return new StringBuffer("");
                }

                @Override
                public StringBuffer format(long value, StringBuffer buffer, FieldPosition field) {
                    throw new UnsupportedOperationException("Not yet implemented.");
                }

                @Override
                public Number parse(String string, ParsePosition position) {
                    throw new UnsupportedOperationException("Not yet implemented.");
                }
            });
            updatePlot(numbers);
            getActivity().setProgressBarIndeterminateVisibility(Boolean.FALSE);
        }
    }
}