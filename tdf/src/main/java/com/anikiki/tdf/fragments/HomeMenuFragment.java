package com.anikiki.tdf.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.anikiki.tdf.R;
import com.anikiki.tdf.activities.StageActivity;
import com.anikiki.tdf.activities.TeamsAndRidersActivity;
import com.anikiki.tdf.utils.Utils;


/**
 * Created by Ana on 7/3/2014.
 */
public class HomeMenuFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        if (getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            if (Utils.isLargeScreen(getActivity())) {
                rootView.findViewById(R.id.home_image).setVisibility(View.VISIBLE);
            } else {
                rootView.findViewById(R.id.disclaimer_fill_space).setVisibility(View.VISIBLE);
            }
        }
        Button teamsAndRidersButton = (Button) rootView.findViewById(R.id.teams_riders_button);
        teamsAndRidersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TeamsAndRidersActivity.class);
                startActivity(intent);
            }
        });
        Button stageOneButton = (Button) rootView.findViewById(R.id.stage_one_button);
        stageOneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), StageActivity.class);
                intent.putExtra(StageActivity.ROUTE_FILE_ID, R.raw.routeone);
                intent.putExtra(StageActivity.VIDEO_ID, getString(R.string.stage_one_video_id));
                intent.putExtra(StageActivity.STAGE_NUMBER, 0);
                intent.putExtra(StageActivity.STAGE_NAME, getString(R.string.stage_one_text));
                startActivity(intent);
            }
        });
        Button stageTwoButton = (Button) rootView.findViewById(R.id.stage_two_button);
        stageTwoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), StageActivity.class);
                intent.putExtra(StageActivity.ROUTE_FILE_ID, R.raw.routetwo);
                intent.putExtra(StageActivity.VIDEO_ID, getString(R.string.stage_two_video_id));
                intent.putExtra(StageActivity.STAGE_NUMBER, 1);
                intent.putExtra(StageActivity.STAGE_NAME, getString(R.string.stage_two_text));
                startActivity(intent);
            }
        });
        Button stageThreeButton = (Button) rootView.findViewById(R.id.stage_three_button);
        stageThreeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), StageActivity.class);
                intent.putExtra(StageActivity.ROUTE_FILE_ID, R.raw.routethree);
                intent.putExtra(StageActivity.VIDEO_ID, getString(R.string.stage_three_video_id));
                intent.putExtra(StageActivity.STAGE_NUMBER, 2);
                intent.putExtra(StageActivity.STAGE_NAME, getString(R.string.stage_three_text));
                startActivity(intent);
            }
        });
        return rootView;
    }
}
