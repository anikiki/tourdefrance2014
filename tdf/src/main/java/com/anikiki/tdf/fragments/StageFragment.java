package com.anikiki.tdf.fragments;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.anikiki.tdf.R;
import com.anikiki.tdf.application.TdfApplication;
import com.anikiki.tdf.models.Stage;
import com.anikiki.tdf.utils.FileUtils;

import org.json.JSONObject;

/**
 * Created by Ana on 7/4/2014.
 */
public class StageFragment extends Fragment implements HandleBackKey {
    private static final String TAG = StageFragment.class.getSimpleName();
    private static final String ALARM_DIALOG_TAG = "alarm_dialog";
    public static final String STAGE_NUMBER = "stage_number";

    private View rootView;
    private LoadStageInfoTask loadStageInfoTask;
    private FragmentNavigationListener fragmentNavigationListener;
    private Stage currentStage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().invalidateOptionsMenu();
        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.stage_fragment, container, false);
        loadStageInfo();
        return rootView;
    }

    @Override
    public void onDestroyView() {
        cancelLoadStageInfo();
        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.stage_activity_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.alarm_set_menu:
                setAlarm();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setAlarm() {
        if (currentStage == null) {
            return;
        }
        long startStageReminderFromPref = ((TdfApplication) getActivity().getApplication()).getTdfPreferences().getStartStageReminderTime(currentStage.getNumber());
        if (startStageReminderFromPref == 0) {
            startStageReminderFromPref = currentStage.getTimestamp();
        }
        Bundle stageInfoBundle = new Bundle();
        stageInfoBundle.putString(AlarmDatePickerFragment.STAGE_NAME, currentStage.getName());
        stageInfoBundle.putInt(AlarmDatePickerFragment.STAGE_NUMBER, currentStage.getNumber());
        stageInfoBundle.putLong(AlarmDatePickerFragment.STAGE_START, currentStage.getTimestamp());
        stageInfoBundle.putLong(AlarmDatePickerFragment.STAGE_REMINDER_TIME, startStageReminderFromPref);
        AlarmDatePickerFragment alarmDatePickerFragment = new AlarmDatePickerFragment();
        alarmDatePickerFragment.setArguments(stageInfoBundle);
        alarmDatePickerFragment.show(getFragmentManager(), ALARM_DIALOG_TAG);
    }

    @Override
    public boolean handleBackKey() {
        if (fragmentNavigationListener != null) {
            fragmentNavigationListener.handleBackNavigationFromFragment();
            return true;
        }
        return false;
    }

    public void setListener(FragmentNavigationListener fragmentNavigationListener) {
        this.fragmentNavigationListener = fragmentNavigationListener;
    }

    private void loadStageInfo() {
        if (loadStageInfoTask == null || loadStageInfoTask.getStatus() == LoadStageInfoTask.Status.FINISHED) {
            loadStageInfoTask = (LoadStageInfoTask) new LoadStageInfoTask().execute(getArguments().getInt(STAGE_NUMBER));
        } else {
            Toast.makeText(getActivity(), R.string.err_load_info, Toast.LENGTH_LONG).show();
        }
    }

    private void cancelLoadStageInfo() {
        if (loadStageInfoTask != null && loadStageInfoTask.getStatus() == LoadStageInfoTask.Status.RUNNING) {
            getActivity().setProgressBarIndeterminateVisibility(false);
            loadStageInfoTask.cancel(true);
            loadStageInfoTask = null;
        }
    }

    private class LoadStageInfoTask extends AsyncTask<Integer, Void, Stage> {
        @Override
        protected Stage doInBackground(Integer... params) {
            Stage stage = null;
            JSONObject obj = FileUtils.convertFile(R.raw.stages, getActivity());
            if (obj != null) {
                stage = FileUtils.getStageInfo(obj, params[0]);
                currentStage = stage;
            }
            return stage;
        }

        @Override
        protected void onPostExecute(Stage stage) {
            ((TextView) rootView.findViewById(R.id.stage_info_length)).setText(stage.getLength());
            ((TextView) rootView.findViewById(R.id.stage_info_start_date)).setText(stage.getStartDate());
            ((TextView) rootView.findViewById(R.id.stage_info_start_time)).setText(stage.getStartTime());
        }
    }
}
