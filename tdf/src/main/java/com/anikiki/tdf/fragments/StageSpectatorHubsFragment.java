package com.anikiki.tdf.fragments;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.anikiki.tdf.R;
import com.anikiki.tdf.adapters.SpectatorHubsArrayAdapter;
import com.anikiki.tdf.models.SpectatorHub;
import com.anikiki.tdf.models.Stage;
import com.anikiki.tdf.utils.FileUtils;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by Ana on 7/6/2014.
 */
public class StageSpectatorHubsFragment extends Fragment implements HandleBackKey {
    private static final String TAG = StageSpectatorHubsFragment.class.getSimpleName();

    public static final String HUB_MARKER_POSITION = "hub_marker_position";
    public static final String ROUTE_FILE_ID = "route_file_id";
    public static final String STAGE_NUMBER = "stage_number";

    private View rootView;
    private LoadSpectatorHubsForStageTask loadSpectatorHubsForStageTask;
    private StageSpectatorHubsListener stageSpectatorHubsListener;
    private List<SpectatorHub> hubs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        getActivity().invalidateOptionsMenu();
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.spectator_hubs_fragment, container, false);
        loadSpectatorHubs();
        return rootView;
    }

    @Override
    public void onDestroyView() {
        cancelLoadSpectatorHubs();
        super.onDestroyView();
    }

    public void setListener(StageSpectatorHubsListener stageSpectatorHubsListener) {
        this.stageSpectatorHubsListener = stageSpectatorHubsListener;
    }

    private void loadSpectatorHubs() {
        if (loadSpectatorHubsForStageTask == null || loadSpectatorHubsForStageTask.getStatus() == LoadSpectatorHubsForStageTask.Status.FINISHED) {
            loadSpectatorHubsForStageTask = (LoadSpectatorHubsForStageTask) new LoadSpectatorHubsForStageTask().execute(getArguments().getInt(STAGE_NUMBER));
        } else {
            Toast.makeText(getActivity(), R.string.err_load_spectator_hubs, Toast.LENGTH_LONG).show();
        }
    }

    private void cancelLoadSpectatorHubs() {
        if (loadSpectatorHubsForStageTask != null && loadSpectatorHubsForStageTask.getStatus() == LoadSpectatorHubsForStageTask.Status.RUNNING) {
            loadSpectatorHubsForStageTask.cancel(true);
            loadSpectatorHubsForStageTask = null;
        }
    }

    @Override
    public boolean handleBackKey() {
        int position = getArguments().getInt(HUB_MARKER_POSITION);
        if (stageSpectatorHubsListener != null && position >= 0) {
            stageSpectatorHubsListener.setSpectatorHubInfo(hubs.get(position), position, false);
            return true;
        }
        return false;
    }

    private class LoadSpectatorHubsForStageTask extends AsyncTask<Integer, Void, Stage> {
        @Override
        protected Stage doInBackground(Integer... params) {
            Stage stage = null;
            JSONObject obj = FileUtils.convertFile(R.raw.stages, getActivity());
            if (obj != null) {
                stage = FileUtils.getSpectatorHubsForStage(obj, params[0]);
            }
            return stage;
        }

        @Override
        protected void onPostExecute(Stage stage) {
            super.onPostExecute(stage);
            hubs = stage.getSpectatorHubs();
            SpectatorHubsArrayAdapter hubsAdapter = new SpectatorHubsArrayAdapter(getActivity(), stage.getSpectatorHubs());
            ListView listView = (ListView) rootView.findViewById(R.id.hubs);
            listView.setAdapter(hubsAdapter);
            listView.setOnItemClickListener(new ListViewItemClickListener());
            int selectedItem = getArguments().getInt(HUB_MARKER_POSITION, -1);
            if (selectedItem != -1) {
                listView.setSelection(selectedItem);
            }
        }
    }

    private class ListViewItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (stageSpectatorHubsListener != null) {
                stageSpectatorHubsListener.setSpectatorHubInfo((SpectatorHub) parent.getItemAtPosition(position), position, false);
            }
        }
    }

    public interface StageSpectatorHubsListener {
        public void setSpectatorHubInfo(SpectatorHub spectatorHub, int position, boolean animateCamera);
    }
}