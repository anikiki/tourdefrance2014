package com.anikiki.tdf.adapters.items.teams;

import android.content.Context;
import android.content.res.Configuration;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.anikiki.tdf.R;
import com.anikiki.tdf.adapters.TeamsArrayAdapter;
import com.anikiki.tdf.adapters.items.ListItem;
import com.anikiki.tdf.models.Team;
import com.anikiki.tdf.utils.Utils;

/**
 * Created by Ana on 7/21/2014.
 */
public class HeaderTeamItem implements ListItem {
    private Context context;
    private Team team;

    public HeaderTeamItem(Context context, Team team) {
        this.context = context;
        this.team = team;
    }

    @Override
    public int getViewType() {
        return TeamsArrayAdapter.ItemType.HEADER_ITEM.ordinal();
    }

    static class ViewHolder {
        TextView nameLine;
    }

    @Override
    public View getView(View convertView) {
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = layoutInflater.inflate(R.layout.header_list_teams_fragment, null);
            if (Utils.isLargeScreen(context) && context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                rowView.setBackground(context.getResources().getDrawable(R.drawable.team_list_item_background_large));
            }
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.nameLine = (TextView) rowView.findViewById(R.id.header_name_line);
            rowView.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder) rowView.getTag();
        TeamsArrayAdapter.populateTextView(holder.nameLine, team.getName(), false);
        return rowView;
    }

    @Override
    public Object getItem() {
        return team;
    }
}
