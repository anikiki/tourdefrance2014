package com.anikiki.tdf.adapters;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.anikiki.tdf.adapters.items.ListItem;

import java.util.List;

/**
 * Created by Ana on 7/4/2014.
 */
public class TeamsArrayAdapter extends ArrayAdapter<ListItem> {
    private List<ListItem> items;

    public enum ItemType {
        ROW_ITEM,
        HEADER_ITEM
    }

    public TeamsArrayAdapter(Context context, List<ListItem> items) {
        super(context, 0, items);
        this.items = items;
    }

    @Override
    public int getViewTypeCount() {
        return ItemType.values().length;
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getViewType();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return items.get(position).getView(convertView);
    }

    public static void populateTextView(TextView textView, String value, boolean hasStyle) {
        if (value == null) {
            value = "";
        }
        if (hasStyle) {
            SpannableString content = new SpannableString(value);
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            textView.setText(content);
        } else {
            textView.setText(value);
        }
    }
}
