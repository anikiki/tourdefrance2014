package com.anikiki.tdf.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.anikiki.tdf.R;
import com.anikiki.tdf.models.Rider;

import java.util.List;

/**
 * Created by Ana on 7/4/2014.
 */
public class RidersArrayAdapter extends ArrayAdapter<Rider> {
    private Context context;
    private List<Rider> values;

    public RidersArrayAdapter(Context context, List<Rider> values) {
        super(context, R.layout.row_list_riders_fragment, values);
        this.context = context;
        this.values = values;
    }

    static class ViewHolder {
        TextView nameLine;
        TextView countryLine;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.row_list_riders_fragment, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.nameLine = (TextView) rowView.findViewById(R.id.name_line_rider);
            viewHolder.countryLine = (TextView) rowView.findViewById(R.id.country_line_rider);
            rowView.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder) rowView.getTag();
        populateTextView(holder.nameLine, values.get(position).getName());
        populateTextView(holder.countryLine, values.get(position).getCountry());
        return rowView;
    }

    private void populateTextView(TextView textView, String value) {
        if (value == null) {
            value = "";
        }
        textView.setText(value);
    }
}
