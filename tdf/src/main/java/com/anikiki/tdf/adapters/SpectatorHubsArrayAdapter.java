package com.anikiki.tdf.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.anikiki.tdf.R;
import com.anikiki.tdf.models.SpectatorHub;

import java.util.List;

/**
 * Created by Ana on 7/6/2014.
 */
public class SpectatorHubsArrayAdapter extends ArrayAdapter<SpectatorHub> {
    private Context context;
    private List<SpectatorHub> values;

    public SpectatorHubsArrayAdapter(Context context, List<SpectatorHub> values) {
        super(context, R.layout.row_list_spectator_hubs_fragment, values);
        this.context = context;
        this.values = values;
    }

    static class ViewHolder {
        TextView nameLine;
        TextView capacityLine;
        TextView facilitiesLine;
        TextView latLine;
        TextView longLine;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.row_list_spectator_hubs_fragment, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.nameLine = (TextView) rowView.findViewById(R.id.name_line_hub);
            viewHolder.capacityLine = (TextView) rowView.findViewById(R.id.capacity_line_hub);
            viewHolder.facilitiesLine = (TextView) rowView.findViewById(R.id.facilities_line_hub);
            viewHolder.latLine = (TextView) rowView.findViewById(R.id.lat_line_hub);
            viewHolder.longLine = (TextView) rowView.findViewById(R.id.long_line_hub);
            rowView.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder) rowView.getTag();
        SpectatorHub hub = values.get(position);
        populateTextView(holder.nameLine, hub.getName());
        populateTextView(holder.capacityLine, hub.getCapacity());
        populateTextView(holder.facilitiesLine, hub.getFacilities());
        populateTextView(holder.latLine, String.valueOf(hub.getLatLong().latitude));
        populateTextView(holder.longLine, String.valueOf(hub.getLatLong().longitude));
        return rowView;
    }

    private void populateTextView(TextView textView, String value) {
        if (value == null) {
            value = "";
        }
        textView.setText(value);
    }
}
