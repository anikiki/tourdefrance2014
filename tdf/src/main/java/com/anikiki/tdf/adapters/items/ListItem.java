package com.anikiki.tdf.adapters.items;

import android.view.View;

/**
 * Created by Ana on 7/21/2014.
 */
public interface ListItem {
    public int getViewType();

    public View getView(View convertView);

    public Object getItem();
}
