## Android app - Tour de France in UK 2014 ##
 


Video and screenshots of the App are available below, or in [this repository location](https://bitbucket.org/anikiki/tourdefrance2014/src/ffe5e83e0a2582b10bc9a8bd2b9e493cf32e83be/video%20and%20screenshots/?at=master):
 

* [Tour de France App video](http://youtu.be/zv1o0gZSjUI)


* [Tour de France App screenshots](https://picasaweb.google.com/116758738566771148390/TourDeFranceApp?authkey=Gv1sRgCM3j6OSNjM3URg)


Tour de France in UK presents the first three stages of the tour which were held in England and it is optimised for both phone and tablet. The application was developed for the sole purpose of getting familiar with Android platform, best practices and responsive application design. It was never designed for a public release in Google Play and might not be bug free.

**Features:**

* see all teams and riders
* check the route of the tour for each stage
* ability to display on the map all the official spectator hubs along the route, the mountain passes and sprints
* check attractions around markers on the map
* option to navigate to a point of interest on the map, navigation starting from current location
* information about spectator hubs and ability to quickly view their location on the map
* elevation profile for each stage
* watch official presentation video for each stage

**The application uses the following APIs:**

* Google Maps 
* Google Places API
* Google Location API
* Google YouTube Android Player API
* Android Plot API
* Sqlite to cache attractions around point of interest